<?php  
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
class iWebsite_Discount_To_All_Products {
	public $settings;
	public $has_user_role;
	public $role_discount_type_in_terms;
	public $role_in_discount_amount_in_terms;
	public $locale;
	public $total_discount_email;
	public $localization_domain;
	public $option_name;
	public $default_settings;
	public $another_discounts;
	
	public function __construct( $language = 'he' ){		
		
		$this->locale 				= '';
		$this->localization_domain 	= ( defined( 'ICL_LANGUAGE_CODE' ) )? ICL_LANGUAGE_CODE : '';
		$this->total_discount_email	= 0;
		$this->option_name 			= 'iwebsite_all_products';
		$this->default_settings 	= array( 
			'active_sale' 			=> '',
			'interaction_with_another_discounts' => '',
			'discount_value'		=> '',
			'discount_measure'		=> '',
			'image' 				=> '',
			'discount_text' 		=> '',
			'discount_background' 	=> '',
		);
		$this->settings				= $this->get_options();
		$discount 					= $this->settings['discount_value'];
		$active_sale 				= $this->settings['active_sale'];
		$this->another_discounts 	= $this->settings['interaction_with_another_discounts']; 
		
		add_action('admin_enqueue_scripts', array( $this, 'enqueue_admin_js' ) );
		
		// if ( isset( $_GET['test'] ) ) show( is_wc_endpoint_url('orders'), 'is_wc_endpoint_url()()' );  
		if( !is_admin() && $active_sale && !empty( $discount ) && !is_account_page() ){	
			$this->add_filters();
		} 
	}
	public function add_filters(){
		
		add_action( 'woocommerce_before_calculate_totals', 	array( $this, 'change_price_test' ), 140, 1 );
		// filter to output html price in shop / product-category / single product
		add_filter( 'woocommerce_get_price_html',  			array( $this, 'get_discount_page_types' ), 111, 2 );

		// filter output subtotal price for each cart item on cart/checkout pages ( $price_product * $quantity )
		add_filter( 'woocommerce_cart_item_subtotal', 		array( $this, 'calculate_cart_item_subtotals' ), 111, 3 );

		// filter displaying price on minicart 
		add_filter( 'woocommerce_cart_item_price', 			array( $this, 'cart_item_price_html' ), 111, 3 );
		
		add_action( 'custom_discount_cart_hook', 			array( $this, 'discount_label_to_product_in_cart'), 111 );
		add_action( 'woocommerce_email_after_order_table', 	array( $this, 'discount_in_email') );



	}
	public function change_price_test( $cart ) {

		//  Exit function if price is changed at backend
		if ( is_admin() && ! defined( 'DOING_AJAX' ) )
			return;

		foreach ( $cart->get_cart() as $key => $item ) {
			$variation_id 			= $item['data']->get_id();
			$product_id 			= wp_get_post_parent_id( $variation_id );
			$product 				= wc_get_product( $product_id );
			if( !isset( $product ) ) continue;
			$product_price 			= $product->get_price();
		
			$price_after_discount 	= $this->get_discount_of_product( $product_id, true );
			//show( $price_after_discount, '$price_after_discount total');
			$discount_amount		= 0;
			
			if ( $price_after_discount ){				
				$discount_amount 	+= ( $product_price - $price_after_discount );
		    } 			
			$total 	= $product_price - $discount_amount; 
			//show( $total, '$total total');
			$item['data']->set_price(  round  ( $total ) );
		}
	}
	
	// Set discount price for simple product 
	public function get_discount_of_product( $product_id = '', $minicart = false ){
		global $product, $woocommerce, $post;
		if( empty( $product_id ) ){
			$product_id = $post->ID;
		}
		//show( $product_id );
		$discount_percent 	= $this->settings['discount_value']; 
		if( isset( $discount_percent ) && !empty( $discount_percent ) ){	
			$price	= "";
			if( $minicart ){
				$price .= $this->change_html_sale_price(  $product_id, true  );
			} elseif( is_product() ) {					
				// $price .= '<p class="price">';
				$price .= $this->change_html_sale_price( $product_id ); 
				// $price .=  '</p>';	
			} else {
				$price .= $this->change_html_sale_price( $product_id );
			}
			return $price;
		}
		return false;
	}
	
	// Main filter collect price
	public function change_html_sale_price(  $product_id = '', $minicart = false ){	
		global $product, $woocommerce, $post;


		if ( $product_id == '' && $product ){
			$product_id 		= $product->get_id(); 
		}
		$discount_image 		= ( isset( $this->settings[ 'image' ] ) && $this->settings[ 'image' ] !='' )? $this->settings[ 'image' ] : false;
		$discount_text 			= $this->settings[ 'discount_text' ];
		$discount_percent 		= $this->settings[ 'discount_value' ];
		$discount_bg 			= $this->settings[ 'discount_background' ];

		$product 				= wc_get_product( $product_id );
		if( $discount_image && !empty(  $discount_image )  ){
			$discount_image = wp_get_attachment_url( $discount_image );
			$discount_html	= '<img class="discount-description" src="'.$discount_image.'" alt="" />';
		} elseif( isset( $discount_text ) && !empty( $discount_text ) ) {
			$discount_html	= '<p class="discount-description" style="background: '.$discount_bg.'">'.$discount_text.'</p>';
		} else {
			$discount_html	= '';
		}
		$currency 		= get_woocommerce_currency_symbol();
		$is_cart 		= false;
		$user_discount	= 0;

		if( is_cart() || is_checkout() ){
			$is_cart 	= true;
		} 
	
		if ( $product->is_type( 'variable' ) ){
			$regular_price 	= $product->get_variation_regular_price();
			$sale_price 	= $product->get_variation_sale_price();
		} else { 
			$regular_price	= $product->get_regular_price();
			$sale_price 	= $product->get_sale_price();
		}

		$price 				= $product->get_price();
		$price_before		= $price ;
		if ( $this->another_discounts ){
			// $price -= $price - $this->get_user_role_additional_discount( $product_id );
			$additional_category_discount = $this->get_additional_sale_price_for_product_by_cat( $product_id );
			if ( $additional_category_discount )
				$price -= $price - $additional_category_discount;
		}
		// === Formula === //
		$price_after_discount 		= $price - $price * $discount_percent/100;
		

		// $price_after_discount_user	=  round( $price_after_discount - $user_discount );
		$price_after_discount_user	=  round( $price_after_discount );

		//$this->total_discount_email	+= $price_before - $price_after_discount;
		$price 	= "";
		if ( is_product_category() && $regular_price != $price_before ){
			$price .= '<del>'.wc_price( $regular_price ).'</del>';
		}	
		// if ( is_product() ){
		// 	$price .= '<del>'.wc_price( $regular_price ).'</del>';
		// }
		$price .= '<p class="price-before">'.wc_price( $price_before ).'</p>';
		$price .= '<div class="price-on-image">';
		if ( $discount_html != '' )  $price .= '<div>'.$discount_html.'</div>';
		$price .= '<div style="background-color: '.$discount_bg.'">'.wc_price( $price_after_discount_user ).'</div>';
		$price .= '</div>';

		//show( $price_after_discount, '$price_after_discount' );
		if( $is_cart || $minicart ){ // Return only price of product without html
			return $price_after_discount_user;
		} else {
			return $price;
		}
	}

	// Set price depend on type of page
	public function get_discount_page_types( $price, $instance ){
		
		global $product, $woocommerce, $post;
		$product_id = $instance->get_id();
		$price_after_discount = $this->get_discount_of_product( $product_id );
		if( isset( $price_after_discount ) &&  $price_after_discount ){
			return $price_after_discount;
		} else {
			return $price;
		}
	}

	// output subtotal price for each cart item on cart/checkout pages ( $price_product * $quantity )
	public function calculate_cart_item_subtotals( $total_price, $cart_item, $cart_item_key  ){
		$user_discount 			= 0;
		$product_id				= $cart_item['product_id'];
		$price_after_discount 	= $this->get_discount_of_product( $product_id );
		$price 					= $this->get_product_price_by_id( $product_id );
		// if ( $this->another_discounts ){
		// 	$user_discount = $price - $this->get_user_role_additional_discount( $product_id );
		// }
		if ( $price_after_discount ){
			$total_price 		= $price_after_discount  * $cart_item['quantity'];
			$total_price 		= wc_price( $total_price );
	    } 		
		//show( $total_price );
		return $total_price;
	}

	// Mini cart 
	public function cart_item_price_html( $price, $cart_item, $cart_item_key ){
		$discount_amount		= 0;
		$qty 					= 1;
		$product_id 			= $cart_item['product_id'];
		$qty 					= $cart_item["quantity"];
		$product 				= wc_get_product( $product_id );
		$product_price			= $product->get_price();		
		$price_after_discount 	= $this->get_discount_of_product( $product_id, true );

		if ( $price_after_discount ) {
			$final_price 	= $price_after_discount;
		} else {
			$final_price 	= $product_price;
		}

		$price 					= wc_price( $final_price );
		return $price;
	}
	
	public function cart_calculate_subtotals( $price, $compound, $obj ){
		$price_number = str_replace("₪","", $price );
		if(  $price_number !== 0 ) return $price;
		$subtotal 					= $obj->subtotal;
		$total_price 				= 0;	
		$total_before_discount		= 0;
		$discount_amount			= 0;

		foreach ( WC()->cart->cart_contents as $key => $cart_item ) {
			$product_id 			= $cart_item['product_id'];
			$quantity 				= $cart_item['quantity'];
			$product 				= wc_get_product( $product_id );
			$product_price			= $product->get_price();
			if( $subtotal == 0 ){
				$total_price += $product_price * $quantity;
			}
			$price_after_discount	= $this->get_discount_of_product( $product_id, true );
			if ( $price_after_discount ){
				$discount_amount 	+= ( $product_price - $price_after_discount  ) * $quantity ;
		    } else {
				$discount_amount 	+= 0;
			}
		}
		if( $subtotal == 0 ){
			$subtotal 				= $total_price;
		}
		$total_price				= $subtotal - $discount_amount ;
		if ( $total_price > 0 ) {
			$price 				= wc_price( $total_price );
		} 
		//show( $price, 'price' );
		return $price;
	}
	
	public function change_cart_totals_order_total_html( $price, $cart_instance ){
		$total			 = 0;
		$discount_amount = 0;
		$shipping_total  = 0;
		$dp_total 		 = 0;
		$fee_total 		 = 0;
		$shipping_total  = $cart_instance->shipping_total;
		$fee_total 		 = $cart_instance->fee_total;
		$dp_total 		 = $cart_instance->dp;
		$additional 	 = $shipping_total + $fee_total + $dp_total;
		
		foreach ( $cart_instance->cart_contents as $key => $cart_item ) {
			$product_id 			= $cart_item['product_id'];
			$item_quantity	 		= $cart_item["quantity"];

			$product 				= wc_get_product( $product_id );
			$product_price 			= $product->get_price();
		
			$price_after_discount 	= $this->get_discount_of_product( $product_id );
			
			if ( $price_after_discount ){				
				$discount_amount 	+= ( $product_price - $price_after_discount ) * $item_quantity;
		    } 
			$total 					+= $product_price * $item_quantity ; 
		}
		$new_price = $total - $discount_amount + $additional;
		
		return $new_price;
	}

	public function discount_in_email( $order ){
		$discount_total = $this->settings['discount_for_email'];
		if( isset( $discount_total ) && !empty( $discount_total ) ){
			$discount_text 	= $this->settings['text'];
			$discount_bg 	= $this->settings['background'];
			$cart_total 	= intval( $order->get_total() );
			$discount_pay 	= 1 + ( $discount_total / 100 );
			$discount		= $cart_total * $discount_pay - $cart_total;
			echo '<div style="margin:10px 0;font-size:1.2rem;color:'.$discount_bg.';">'. sprintf( __( $discount_text.'- %d', IWEBSITE_SALE_NAME ), $discount ) . '</div>';
		}
	}

	public function discount_label_to_product_in_cart(){
		$discount_total = ( $this->settings['discount_value'] !== null )? $this->settings['discount_value'] : false;
		if( isset( $discount_total ) && !empty( $discount_total ) ){
			$discount_text 	= isset( $this->settings['text'] ) && $this->settings['text'] != '' ? $this->settings['text'] : false;
			$discount_bg 	= isset( $this->settings['background'] ) && $this->settings['background'] != '' ? $this->settings['background'] : false;
			$discount_image 	= ( isset( $this->settings[ 'image' ] ) && $this->settings[ 'image' ] != '' )? wp_get_attachment_url( $this->settings[ 'image' ] ) : false;
			// $discount_image	= $this->settings['image'];
			if( isset( $discount_image ) && !empty( $discount_image )  ){
				$discount_html	= '<img class="discount-description" src="'.$discount_image.'" alt="" />';
			} elseif( isset( $discount_text ) && !empty( $discount_text ) ) {
				$discount_html	= '<p class="discount-description" style="background: '.$discount_bg.'">'.$discount_text.'</p>';
			}
			echo $discount_html;
		}	
	}

// new code

		// Output admin subpage with settings form 
	public function admin_page_settings(){
		// show( $this->settings , 'ALL PRODUCTS SETTINGS' );
		if ( isset( $this->settings ) && !empty( $this->settings  ) ){

			$sale_end 			= ( isset( $this->settings['sale_end'] ) )? $this->settings['sale_end'] : date("y-m-d") ;
			$sale_start 		= ( isset( $this->settings['sale_start'] ) )? $this->settings['sale_start'] : date("y-m-d") ;
			$discount_value 	= ( isset( $this->settings['discount_value'] ) )? $this->settings['discount_value'] : '';
			$discount_measure 	= ( isset( $this->settings['discount_measure'] ) ) ? $this->settings['discount_measure'] : 'percent' ;
			$active_sale 		= ( isset( $this->settings['active_sale'] ) )? $this->settings['active_sale'] : '';
			$image 				= ( isset( $this->settings['image'] ) )? $this->settings['image'] : '';
			$discount_image 	= wp_get_attachment_image_src( $image );
			$discount_text 		= ( isset( $this->settings['discount_text'] ) )? $this->settings['discount_text'] : '';
			$discount_background = ( isset( $this->settings['discount_background'] ) )? $this->settings['discount_background'] : '';
			$interaction 		= ( isset( $this->settings['interaction_with_another_discounts'] ) )? $this->settings['interaction_with_another_discounts'] : '';
?>		
				<h3 class="section-title">
					<?php _e( 'Sale for all products', IWEBSITE_SALE_NAME ); ?>
					<input type="hidden" name="all_product_settings" value="1">
				</h3>
				<div id="all-products-sale" class="sale-repeater">	
					<div class="clearfix">
						<div class="section-discount-content">
							<div class="active-sale">
								<div class="row-title dib">
									<strong><?php _e( 'Active sale', IWEBSITE_SALE_NAME ); ?></strong>
								</div>
								<div class="row-content dib">
									<label for="first_product_discount_label">
										<input type="checkbox" name="active_sale" value="true" <?php if ( $active_sale ) echo 'checked="cheked"'; ?>>
									</label>
								</div>
							</div>	
							<div class="interaction-sale">
								<div class="row-title dib">
									<strong><?php _e( 'Interaction with another sales', IWEBSITE_SALE_NAME ); ?></strong>
								</div>
								<div class="row-content dib">
									<label for="first_product_discount_label">
										<input type="checkbox" name="interaction_with_another_discounts" value="true" <?php if ( $interaction ) echo 'checked="cheked"'; ?>>
									</label>
								</div>
							</div>																
							<div class="discount-value">
								<div class="row-title dib">	
									<strong><?php _e( 'Discount value', IWEBSITE_SALE_NAME ); ?></strong>
								</div>
								<div class="row-content dib">
									<label for="discount_value">
										<input type="text" name="discount_value" value="<?php echo $discount_value ?>" >
									</label>
								</div>
							</div>
							<div class="discount-measure">
								<div class="row-title dib">
									<strong><?php _e( 'Discount measure', IWEBSITE_SALE_NAME ); ?></strong>
								</div>
								<div class="row-content dib">
									<label for="percent" class="b">
										<input type="radio" name="discount_measure" value="percent" id="percent" <?php if ( $discount_measure == 'percent' || $discount_measure == '' ) echo 'checked="checked"'; ?>>
										<span><?php _e( 'percent', IWEBSITE_SALE_NAME ) ?></span>
									</label>
									<label for="fixed" class="b">
										<input type="radio" name="discount_measure" value="fixed" id="fixed" <?php if ( $discount_measure == 'fixed' ) echo 'checked="checked"'; ?>>
										<span><?php _e( 'fixed', IWEBSITE_SALE_NAME ) ?></span>
									</label>
								</div>
							</div>
				           <div class="discount-image">
				                <div class="row-title dib <?php if ( $discount_image[0] != '' ) echo 'expand'; ?>">
				                  <strong><?php _e( 'Discount image banner:', IWEBSITE_SALE_NAME );?></strong>
				                </div>
				                <div class="row-content dib">
				                  <span class="image-tooltip"><?php _e( 'No image selected', IWEBSITE_SALE_NAME ); ?></span>
				                  <img src="<?php echo $discount_image[0]; ?>" class="custom-img">
				                  <input type="hidden" id="custom-img" name="image" value="<?php echo $image; ?>">  
				                  <button class="set-custom-img button"><?php _e( 'Set discount image', IWEBSITE_SALE_NAME ); ?></button>
				                  <button class="remove-custom-img button">-</button>
				                </div>
				            </div> 
  							<div class="discount-text">
				                <div class="row-title dib">
				                  <strong><?php _e( 'Discount text:', IWEBSITE_SALE_NAME );?></strong>
				                </div>
				                <div class="row-content dib">
				                  <input type="text" name="discount_text" value="<?php echo $discount_text; ?>" >				           
				                </div>
				            </div> 	
  							<div class="discount-background">
				                <div class="row-title dib">
				                  <strong><?php _e( 'Discount background:', IWEBSITE_SALE_NAME ); ?></strong>
				                </div>
				                <div class="row-content dib">
				                  <input type="color" name="discount_background" value="<?php echo $discount_background; ?>" >
				                </div>
				            </div> 				            
							<?php //iwebsite_date_pickup( $this->settings, 0 ); ?>
						</div>
					</div>
				</div>	
<?php	
			}		
	}

	// get discount settings for current tab in admin page 
    public function get_options() {

    	if ( !$options = get_option( $this->option_name ) ) {
	        $options = $this->default_settings ;
    		update_option( $this->option_name, $options );
    	}
    	// if empty value was saved
    	$options = array_merge( $this->default_settings, $options );
   		return $options;
    } 

	public function set_discount_settings( $new_settings ){
		$this->settings = $new_settings;
		return $this->settings;
	}

	
// New methods for interaction with another discounts
	// get userrole discount
	public function get_user_role_additional_discount( $product_id  ){
    	$current_user 		= wp_get_current_user();
		$current_user_roles = $current_user->roles;
		$price 				= $this->get_product_price_by_id( $product_id );
		$sale_price 		= false;
		$ar_settings 		= get_option( 'iwebsite_sale_for_userrole'.$this->locale );
		if ( is_array( $current_user_roles ) ){
			$settings 		= false;
			if ( $ar_settings ){
				foreach( $ar_settings as $repeater ){
					// show( $repeater, 'repeater' );
					$active_sale = isset( $repeater['active_sale'] )? $repeater['active_sale'] : false;
					$userrole 	= $repeater['userrole'];
					$discount 	= ( isset( $repeater['discount_value'] ) && $repeater['discount_value'] != '' )? $repeater['discount_value'] : false;
					if ( !$discount || !$active_sale ) continue;
					$measure 	= ( isset( $repeater['discount_measure'] ) && $repeater['discount_measure'] != '' ) ? $repeater['discount_measure'] : 'percent' ;
					if ( in_array( $userrole, $current_user_roles ) ){
						$settings['discount'] = $discount;
						$settings['measure'] = $measure;
						break;
					}
				}
			}
			if ( is_array( $settings ) && !empty( $settings ) ){
				$discount_measure	= $settings['measure'];
				$discount			= $settings['discount'];
				$discount_price 	= ( $discount_measure == 'percent' )? ( $price * $discount / 100 ) : $discount;
				$sale_price 		= round( $price - $discount_price );
				// $sale_price 		= $discount_price;
			}
		}
		// show( $sale_price, 'sale_price for administrator' );
		return $sale_price;
	}

	public function get_product_price_by_id( $product ){
		if ( is_numeric( $product ) ){
			$product = wc_get_product( $product );
		} 
		$price = false;
		if ( $product ){
			if ( $product->is_type( 'variable' ) ){
				$price = ( $product->get_variation_sale_price() )? $product->get_variation_sale_price() : $product->get_variation_regular_price() ;
			} else { 
				$price = ( $product->get_sale_price() ) ? $product->get_sale_price() : $product->get_regular_price();
			}
		}

		return $price;
	}


	public function get_additional_sale_price_for_product_by_cat( $product  ){
		if ( is_numeric( $product ) ){
			$product_id = $product;
		} else {
			if ( $product->is_type( 'simple' ) ){
				$product_id =  $product->get_id(); 
			} else { 
				$product_id =  $product->get_parent_id(); 
			}
		}
		$price 					= $this->get_product_price_by_id( $product );
		$userrole_price 		= $this->get_user_role_additional_discount( $product_id );
		if ( $userrole_price ){
			$price -= $userrole_price;
		}
		// show( $price, '$price from userrole discount' );
		$sale_price 		= false;
		$product_categories = get_the_terms( $product_id, 'product_cat' );
		if ( $product_categories ){
			foreach ( $product_categories as $key => $product_cat ) {
				$product_category[] = $product_cat->term_id;
			}
			$sale_repeater = get_option( 'iwebsite_discount_categories' );
			$settings = false;
			if ( $sale_repeater ){
				foreach( $sale_repeater as $repeater ){
					$discount_categories 	= ( isset( $repeater['sales_categories'] ) && !empty( $repeater['sales_categories'] ) )?  $repeater['sales_categories'] : false ;
					$discount 				= ( isset( $repeater['discount_value'] ) && !empty( $repeater['discount_value'] ) ) ? $repeater['discount_value'] : false ;
					$measure 				= ( isset( $repeater['discount_measure'] ) ) ? $repeater['discount_measure'] : 'percent' ;
					$active_sale 			= ( isset( $repeater['active_sale'] ) ) ? $repeater['active_sale'] : false ;
					
					if ( !$active_sale || !$discount_categories || !$discount ){
						continue;
					}
					foreach ( $product_categories as $key => $product_cat ) {	
						$cat_id = $product_cat->term_id;

						if ( in_array( $cat_id , $discount_categories ) ){
							$settings['discount'] = $discount;
							$settings['measure'] = $measure;
							break;
						}
					}
				}
			}		
			// show( $settings, 'settings');
			if ( is_array( $settings ) && !empty( $settings ) ){
				$discount 			= $settings['discount'];
				$discount_measure 	= $settings['measure'];
				$discount_price 	= ( $discount_measure == 'percent' )? ( $price * $discount / 100 ) : $discount;
				$sale_price 		= ceil( $price - intval( $discount_price ) );

			}
		}
		return $sale_price;
	}

	public function enqueue_admin_js() {
		wp_enqueue_media();
		// wp_enqueue_script('media-upload'); //Provides all the functions needed to upload, validate and give format to files.
	}



}
new iWebsite_Discount_To_All_Products();