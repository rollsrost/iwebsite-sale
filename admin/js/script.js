jQuery(function($) {

    function resetAttributeNames(section) { 
        var idAttr = section.attr('id');
        var id = parseInt( idAttr ) + 1;
        var inputs = section.find('input');

        section.attr('id' , id);
        section.find('.delete-row').show();

        console.log(idAttr, 'idAttr');
        console.log(id, 'id');

        inputs.each(function() {
            var $this = $(this);
            var attr_name = $this.attr('name');
            var attr_id = $this.attr('id');
            if (attr_name) {
                var reg = attr_name.replace(/\[\d+\]/, '\['+( id )+'\]');
               $this.attr('name', reg);
                // console.log(attr_name, 'attr_name');
                // console.log(reg, 'reg');
            }
             if (attr_id) {
                var reg = attr_id.replace(/\[\d+\]/, '\['+( id )+'\]');
               $this.attr( 'id', reg );
            }

            $this.removeAttr('checked');
            if (  !$this.hasClass('untouchable') ){
                // console.log(attr_name, 'attr_name SALES')
                $this.val('');
            }
        });
        section.find('.sale_permanent').change();
        section.find('.temporary-sale-date').removeClass('visible');
    }

// Clone the previous section, and remove all of the values                  
    $('.sale-repeater-btn').on( 'click', function(e){

        var lastRepeatingGroup = $('.sale-repeater').last();
        var lastTitle = $('h3.section-title').last();
        var newTitleNumber = lastTitle.find('span.number');
        var newNumber =  parseInt( newTitleNumber.html() ) + 1 ;
        var cloned = lastRepeatingGroup.clone(true) ;
        var clonedTitle = lastTitle.clone(true);
        var id = cloned.attr('id');
        clonedTitle.find('.cats').html('');
        clonedTitle.find('span.number').html( newNumber );
        resetAttributeNames(cloned);
        clonedTitle.insertAfter(lastRepeatingGroup);
        cloned.insertAfter(clonedTitle);
        e.preventDefault();

        // console.log( clonedTitle.find('.cats'), 'title')
        console.log( lastTitle, 'lastTitle');
        console.log( newTitleNumber, 'newTitleNumber');
        console.log( newNumber, 'newNumber');
        console.log( id, 'id');
        console.log( cloned, 'cloned');

    });

    $('button.delete-row').on( 'click', function(e){
        console.log($('.sale-repeater').length, 'test')
        if ( $('.sale-repeater').length > 1 ){
            var repeater = $(this).closest('.sale-repeater');
            repeater.prev().remove();
            repeater.remove(); 
        } else {
            alert('Must be at least 1 value');
        }
        e.preventDefault();
    });

    $(document).on('click', '.show-section', function(){
        // var target = $(this).closest('.'+ $(this).data('key') );
        var target = $(this).closest('.sale-repeater').find('.all-categories-scrolling');
        target.slideToggle();
        return false;
    });

    $('input[name="discount_measure"]').on('change', function(){
        $('input[name="discount_measure"]').attr('checked', null);
        $(this).attr('checked', 'checked');
    });


    $('.custom-img').each( function( i ){
        if ( $(this).attr('src') != ''  ){
            $(this).siblings('.image-tooltip, .set-custom-img').hide();
            $(this).siblings('.remove-custom-img').show();
        }
    })

// additional image loader  used wp media loader
   if ($('.set-custom-img').length > 0) {
        if ( typeof wp !== 'undefined' && wp.media && wp.media.editor) {
            var frame = wp.media.editor;
            var clickedBtn;
            $(document).on('click', '.set-custom-img', function(e) {
                e.preventDefault();
                var button = $(this);
                clickedBtn = button;
                var attId = button.prev();
                var title = clickedBtn.closest('.sale-image').find('.row-title');
                wp.media.editor.send.attachment = function(props, attachment) {
                    attId.val(attachment.id);
                    button.parent().find('img').attr('src', attachment.url );
                    clickedBtn = '';
                    button.siblings('.remove-custom-img').show();
                    button.siblings('.image-tooltip').hide();
                    button.hide();
                    title.addClass('expand');
                };
                wp.media.editor.open(button);
                return false;
            });

            wp.media.view.Modal.prototype.on('close', function(){
                if ( clickedBtn == '' ){
                    return;
                } 
                var img = clickedBtn.siblings('.custom-img');
                if ( img.attr('src') == '' ) {
                    clickedBtn.show();
                    clickedBtn.siblings('.remove-custom-img').hide();
                    clickedBtn.siblings('.image-tooltip').show();
                }
            });

            $(document).on('click', '.remove-custom-img' , function(e) {
                e.preventDefault;
                var parent = $(this).parent();
                var title = $(this).closest('.sale-image').find('.row-title');
                parent.find('#custom-img').val('');
                parent.find('img').attr('src','');
                parent.find('.set-custom-img').show();
                parent.find('.image-tooltip').show();
                title.removeClass('expand');
                $(this).hide();
                return false;
            });
        }
    }

    $('.sale-container-inner').accordion({
        heightStyle: 'content'
    });

    $(document).on( 'change', '.sale_permanent' , function(){
        console.log( 'sale_permanent changed')
        var checked = $(this).prop('checked');
        var tempDate = $(this).closest('.date-pickup').find('.temporary-sale-date');
        $(this).closest('div').find('input').removeAttr( 'checked' );
        $(this).attr( 'checked', 'checked' );
        if ( $(this).val() == 1 ){
            tempDate.slideUp();
        } else {
            tempDate.slideDown();
        } 
    });

    $(document).on('submit', '#sales-categories', function(e){
        $('input.sale_permanent').each(function(index, el){
            var parent = $(el).closest('.date-pickup');
            var target = parent.find('.temporary-sale-date');
            var startDate = target.find('input#start_sale_date');
            var endDate = target.find('input#end_sale_date');

            if ( startDate.val() == '' && endDate.val() == '' ){
                parent.find('input#permanent-yes').click();
            }
        });
        // e.preventDefault();
    })

    $(document).on('change', '.active-third-discount    ', function(){
        var target = $(this).closest('.section-discount-content').find('.third-product-fields');
        if ( $(this).prop('checked') ){
            target.slideDown();
        } else {
            target.slideUp();
        }
    });

    $('.sale-repeater').first().find('.delete-row').hide();

});