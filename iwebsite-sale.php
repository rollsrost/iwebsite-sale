<?php

/*
Plugin Name: iWebsite-sale
Plugin URI: http://iwebsite.co.il
Description: Woocommerce plugin, for different types of discount
Version: 1.0.0
Author: Rostyslav Danyshchuk
Author URI:  http://iwebsite.co.il
Text Domain: iwebsite-sale
Domain Path: /languages
*/

define( 'IWEBSITE_SALES_PATH', dirname( __FILE__ ) );
define( 'IWEBSITE_SALE_NAME', 'iwebsite-sale' );
define( 'IWEBSITE_SALE_VERSION', '0.0.0.1' );


####################################################################################################################
// new type of discount

if ( !class_exists( 'iWebsite_Sale_Init' ) ){
	class iWebsite_Sale_Init {

		public $ar_active_sales;
		//Basic settings for admin

		public function __construct(){
			require_once( IWEBSITE_SALES_PATH .'/iwebsite-admin-page.php'); 
			add_action( 'init', array( $this, 'sale_init'  ) );
			add_action( 'plugins_loaded', array( $this, 'localize_sale_module' ) );
		}

		public function sale_init(){

			$all_discount	= array( 
				// Discount by userrole ( all pages )
				array( 
					'type'			=> 'userroles-discount',
					'class'			=> 'iWebsite_Discount_For_Userroles',
					'label'			=> __( 'User Role Discount' , IWEBSITE_SALE_NAME ),
					'description'	=> __( 'Settings for different userroles settings', IWEBSITE_SALE_NAME ),
					'status'		=> true,
				),				
				// Discount for product category ( all pages )
				array( 
					'type'			=> 'categories-discount',
					'class'			=> 'iWebsite_Discount_For_Categories',
					'label'			=> __( 'Product Category Discount', IWEBSITE_SALE_NAME ),
					'description'	=> __( 'Product Category Discount', IWEBSITE_SALE_NAME ),
					'status'		=> true,
				),
				// Discount by defined order sum reach defined value ( in cart )
				array( 
					'type'			=> 'discount-order-by-some-condition',
					'class'			=> 'iWebsite_Discount_Order_By_Some_Condition',
					'label'			=> __( 'Discount order by some condition', IWEBSITE_SALE_NAME ),
					'description'	=> __( 'Discount order by some condition', IWEBSITE_SALE_NAME ),
					'status'		=> true,
				),	
				// Discount for n ( 2 or 3 ) products from one category ( in cart )
				array( 
					'type'			=> 'first-second-third-products-discount',
					'class'			=> 'iWebsite_First_Second_Third_Product_Discount',
					'label'			=> __( 'Stepped discount for product from 1 category', IWEBSITE_SALE_NAME ),
					'description'	=> __( 'Stepped discount for product from 1 category ( for first product x%, second y%, third z%  )', IWEBSITE_SALE_NAME ),
					'status'		=> true,
				),
				// Discount for n ( 2 or 3 ) products from one category ( in cart )
				array( 
					'type'			=> 'items-per-order-for-same-category',
					'class'			=> 'iWebsite_Items_Per_Order_For_Same_Category',
					'label'			=> __( 'Discount for products from 1 category', IWEBSITE_SALE_NAME ),
					'description'	=> __( 'Discount for products from 1 category ( in cart )', IWEBSITE_SALE_NAME ),
					'status'		=> true,
				),
				// Discount for all products on site
				array( 
					'type'			=> 'discount-to-all-products',
					'class'			=> 'iWebsite_Discount_To_All_Products',
					'label'			=> __( 'Discount for all products on site', IWEBSITE_SALE_NAME ),
					'description'	=> __( 'Discount for all products on site', IWEBSITE_SALE_NAME ),
					'status'		=> true,
				),
				// Fixed price discount for all products in category
				array( 
					'type'			=> 'fixed-price-for-category',
					'class'			=> 'iWebsite_Fixed_Price_For_Category',
					'label'			=> __( 'Fixed price for all products in category', IWEBSITE_SALE_NAME ),
					'description'	=> __( 'Fixed price for all products in category', IWEBSITE_SALE_NAME ),
					'status'		=> true,
				)						
			);

			$ar_active_sales = array();

			// include 
			foreach( $all_discount as $discount ){
				if( $discount['status'] ){

					$class_name = IWEBSITE_SALES_PATH . '/iwebsite-'.$discount['type'].'.php';
					require_once( $class_name );

					$class = $discount['class'];

					// init class for front-end
					if ( class_exists( $class ) ){
						$class_object = new $class( array() );
					}
					$ar_active_sales[]	= $discount;
				}
			}
			$admin = new iWebsite_Admin_Page( $ar_active_sales );
		}


		public function localize_sale_module() {
			load_plugin_textdomain( IWEBSITE_SALE_NAME, false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}

	}
}	

if ( class_exists( 'iWebsite_Sale_Init' ) ){
	$class = new iWebsite_Sale_Init();
}	

?>