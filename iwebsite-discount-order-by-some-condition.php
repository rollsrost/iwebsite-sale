<?php  

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class iWebsite_Discount_Order_By_Some_Condition {
	public $sale_settings;
	public $discount_value;
	public $discount_condition;
	public $discount_measure;
	public $locale;
	public $localization_domain;

	public $default_settings; 
	
	public function __construct(){

		$this->locale = ( defined( 'ICL_LANGUAGE_CODE' ) ) ? ICL_LANGUAGE_CODE : get_locale();

		$this->default_settings = 	array(
			'active_sale' => false,
	        'discount_value' =>  '',
	        'discount_condition' =>  '',
	        'discount_measure' => '',
	        'sale_permanent' => true,
	        'sale_start' =>  '',
	        'sale_end' =>  '',
	        'banner' => '',
	        'sales_categories' => ''
	    );

		$this->option_name			= 'iwebsite_discount_order_by_some_condition'.$this->locale;	
		$this->settings 			= $this->get_options();
		// sort( $this->settings );
		// echo '<div style="margin-left:150px">';
		// show( $this->settings, '$this->settings after sorting' );
		// echo '</div>';
		add_action( 'woocommerce_cart_calculate_fees' , array( $this , 'cart_discount' ) );
	}

	public function cart_discount(){
		if ( is_admin() ) return ;
		global $woocommerce;
		$total_discount 	= 0;
		$discount_product 	= array();

		$cart_total 		= WC()->cart->cart_contents_total;
		$new_order 			= array();
		$order_with_cats	= array();
		foreach ( $this->settings as $setting_key => $setting ) {
			$cats = $setting['sales_categories'];
 			if ( is_array( $cats ) && $cats != '' ){
 				$order_with_cats[ $setting_key ] = $setting['discount_condition'];
 			} else {
				$new_order[ $setting_key ] = $setting['discount_condition'];
			}
		}
		arsort( $order_with_cats );
		arsort( $new_order );
		// show( $new_order, '$new_order' );
		// show( $order_with_cats, '$order_with_cats' );

		if ( is_array( $order_with_cats ) && !empty( $order_with_cats ) ){
			$setting_key = $this->get_setting_key( $order_with_cats );
		} else {
			$setting_key = $this->get_setting_key( $new_order );
		}
		// show( $setting_key, '$setting_key' );
		if ( $setting_key === null ) return;
		$choosed_setting = $this->settings[ $setting_key ];
		// show( $choosed_setting, 'choosed_setting' );
		if ( $choosed_setting['discount_measure'] == 'fixed' ){
			$total_discount = $choosed_setting['discount_value'];
		} else {
			$total_discount = ( $cart_total / 100 ) * $choosed_setting['discount_value'];
		}
		$condition = ( isset( $choosed_setting['discount_condition'] ) && $choosed_setting['discount_condition'] != '' )? intval( $choosed_setting['discount_condition'] ): false;
	

		if ( $choosed_setting['active_sale'] === false || $cart_total <= $condition  ) {
			return;
		}
		$total_discount *= -1; // convert positive to negative fees
		$woocommerce->cart->add_fee( __( 'Cart discount' , IWEBSITE_SALE_NAME ), $total_discount, true, '' ); // add negative fees
		return $total_discount;
	}

		public function admin_page_settings(){
			$currency_symbol = get_woocommerce_currency_symbol();
			$count = 1;
			foreach ( $this->settings as $setting_key => $setting_value ) {
				$sale_end 			= ( isset( $setting_value['sale_end'] ) )? $setting_value['sale_end'] : date("y-m-d") ;
				$sale_start 		= ( isset( $setting_value['sale_start'] ) )? $setting_value['sale_start'] : date("y-m-d") ;
				$discount_value 	= ( isset( $setting_value['discount_value'] ) )? $setting_value['discount_value'] : '';
				$discount_measure 	= ( isset( $setting_value['discount_measure'] ) )? $setting_value['discount_measure'] : 'percent' ;
				$discount_condition = ( isset( $setting_value['discount_condition'] ) )? $setting_value['discount_condition'] : '';
				$discount_sum 		= $discount_condition .$currency_symbol;
				if ( $discount_measure == 'percent' ){
					$discount = 100 - $discount_value;
					$discount .= '%';
				} else {
					$discount = $discount_value . $currency_symbol;
				}
				$real_discount = $discount_condition - $discount_value;
?>		
				<h3 class="section-title">
					<span><?php _e( 'Sale number', IWEBSITE_SALE_NAME ); ?>
					<span class="number"><?php echo $count; ?></span>
					<?php echo sprintf( __( 'Buy on %s pay only %s', IWEBSITE_SALE_NAME ), $discount_sum, $real_discount );  ?></span>
					<span class="toggle-indicator" aria-hidden="true"></span>
				</h3>
				<div id="<?php echo $setting_key; ?>" class="sale-repeater">
					<div class="clearfix">
						<div class="section-discount-content">
							<?php iwebsite_active_sale( $setting_value, $setting_key );  ?>	
							<?php iwebsite_choice_category_section( $setting_value, $setting_key ); ?>		
							<?php iwebsite_add_image( $setting_value, $setting_key ); ?>
							<div>
								<div class="row-title dib">
									<strong><?php _e( 'Discount condition', IWEBSITE_SALE_NAME ); ?></strong>
								</div>
								<div class="row-content dib">
									<label for="discount_condition">
										<input type="text" name="discount_condition[<?php echo $setting_key; ?>]" value="<?php echo $discount_condition; ?>" >
									</label>
								</div>
							</div>
							<div>
								<div class="row-title dib">
									<strong><?php _e( 'Discount measure', IWEBSITE_SALE_NAME ); ?></strong>
								</div>
								<div class="row-content dib">
									<label for="percent" class="b">
										<span><?php _e( 'percent', IWEBSITE_SALE_NAME ) ?></span>
										<input type="radio" name="discount_measure[<?php echo $setting_key; ?>]" value="percent" id="percent" <?php if ( $discount_measure == 'percent' || $discount_measure == '' ) echo 'checked="checked"'; ?>>
									</label>
									<label for="fixed" class="b">
										<span><?php _e( 'fixed', IWEBSITE_SALE_NAME ) ?></span>
										<input type="radio" name="discount_measure[<?php echo $setting_key; ?>]" value="fixed" id="fixed" <?php if ( $discount_measure == 'fixed' ) echo 'checked="checked"'; ?>>
									</label>
								</div>
							</div>								
							<div>
								<div class="row-title dib">
									<strong><?php _e( 'Discount value', IWEBSITE_SALE_NAME ); ?></strong>
								</div>
								<div class="row-content dib">
									<label for="discount_value">
										<input type="text" name="discount_value[<?php echo $setting_key; ?>]" value="<?php echo $discount_value ?>" >
									</label>
								</div>
							</div>
							<?php iwebsite_date_pickup( $setting_value, $setting_key ); ?>
						</div>
						<div class="section-discount-admin">
							<button class="sale-repeater-btn"><span class="label"><?php _e( 'Add row', IWEBSITE_SALE_NAME ); ?></span></button>
							<button class="delete-row"><span class="label"><?php _e( 'Delete row from repeater', IWEBSITE_SALE_NAME ) ?></span></button>
						</div>
					</div>
				</div>
			<?php
			$count++;	
			}		
		}

		// get discount settings for current tab in admin page 
	    public function get_options() {

	    	if ( !$options = get_option( $this->option_name ) ) {
		        $options[] = $this->default_settings ;
	    		update_option( $this->option_name, $options );
	    	}
	    	// if empty value was saved
	    	foreach ( $options as $key => $option ) {
	    		$options[$key] = array_merge( $this->default_settings, $option );
	    	}
	    	// delete_option( $this->option_name );
	   		return $options;
	    } 

		    
		public function set_discount_settings( $new_settings ){
			$this->settings = $new_settings;
			return $this->settings;
		}

		public function get_setting_key( $array ){
			$cart_contents 		= WC()->cart->get_cart();
			$cart_total 		= WC()->cart->cart_contents_total;
			$setting_key = null;
			foreach ( $array as $key => $setting ) {
				$condition 			= intval( $setting );
				$sales_categories 	= $this->settings[$key]['sales_categories'];
				$active_sale 		= $this->settings[$key]['active_sale'];
				if ( $active_sale == false ) continue;
			// show( $this->settings[$key], '$this->settings[$key]' );
				if ( is_array( $sales_categories ) && !empty( $sales_categories ) ){
					$cart_total = 0;
					foreach ( $cart_contents as $cart_key => $cart_content ) {
						$cart_product_id 	= $cart_content['product_id'];
						$total 				= $cart_content['line_total'];
						$product_categories = get_the_terms( $cart_product_id, 'product_cat' );

						if ( $product_categories ){
							foreach ( $product_categories as $cat_key => $product_cat ) {
								$cat_id = $product_cat->term_id;
								if (  in_array( $cat_id, $sales_categories ) ){
									$cart_total += $total;
									break;
								}		
							}
						}	
					}

					if ( $condition <= $cart_total ) {
						$setting_key = $key;
						break;
					} else {
						continue;
					}
				} elseif ( $condition <= $cart_total ) {
					$setting_key = $key;
					break;
				}
			}
			return $setting_key;
		}


/* old
		public function cart_discount(){
			if ( is_admin() ) return ;
			global $woocommerce;
			$total_discount 	= 0;
			$discount_product 	= array();
			$cart_contents 		= WC()->cart->get_cart();
			$cart_total 		= WC()->cart->cart_contents_total;
			$new_order 			= array();

			foreach ( $this->settings as $setting_key => $setting ) {
				$new_order[ $setting_key ] = $setting['discount_condition'] ;
			}
			
			arsort( $new_order );
			show( $new_order, 'new_order' );
			show( $this->settings , 'this->settings ' );

			$setting_key = null;
			foreach ( $new_order as $key => $value ) {
				if ( $value <= $cart_total ){
					$setting_key = $key;
					break;	
				}
			}
			if ( $setting_key === null ) return;
			$choosed_setting = $this->settings[ $setting_key ];
			show( $choosed_setting, 'choosed_setting' );


			if ( $choosed_setting['discount_measure'] == 'fixed' ){
				$total_discount = $choosed_setting['discount_value'];
			} else {
				$total_discount = ( $cart_total / 100 ) * $choosed_setting['discount_value'];
			}
			$condition = ( isset( $choosed_setting['discount_condition'] ) && $choosed_setting['discount_condition'] != '' )? intval( $choosed_setting['discount_condition'] ): false;
		

			if ( $choosed_setting['sales_categories'] != '' || is_array( $choosed_setting['sales_categories'] ) ){
				$cart_total = 0;
				foreach ( $cart_contents as $key => $cart_content ) {
					$cart_product_id 	= $cart_content['product_id'];
					$total 				= $cart_content['line_total'];
					$product_categories = get_the_terms( $cart_product_id, 'product_cat' );
					if ( $product_categories ){
						foreach ( $product_categories as $key => $product_cat ) {
							$cat_id = $product_cat->term_id;
							if (  in_array( $cat_id, $choosed_setting['sales_categories'] ) ){
								$cart_total += $total;
								break;
							}		
						}
					}	
				}

				show( $cart_total, 'cart_total' );
			} 

			if ( $choosed_setting['active_sale'] === false || $cart_total <= $condition  ) {
				return;
			}
			$total_discount *= -1; // convert positive to negative fees
			$woocommerce->cart->add_fee( __( 'Cart discount' , IWEBSITE_SALE_NAME ), $total_discount, true, '' ); // add negative fees
			return $total_discount;
		}
*/

}

?>