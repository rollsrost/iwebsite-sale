<?php  
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class iWebsite_Items_Per_Order_For_Same_Category {
	public $settings;
	public $discount_value;
	public $discount_condition;
	public $discount_measure;
	public $discount_label;

	public $option_name;
	public $default_settings;
	public $localization_domain;

	public function __construct(){

		if ( $this->discount_label === '' ){
			$this->discount_label = __( 'Cart discount', IWEBSITE_SALE_NAME );
		}

		$this->default_settings = 	array(
	        'sales_categories' =>  array(),
	        'quantity_products' =>  '',
	        'price_for_products' =>  '',			            
	        'discount_explanation' => '',
	        'sale_permanent' => true,
	        'sale_start' =>  '',
	        'sale_end' =>  '',
	        'active_sale' => '',
	        'banner' => ''
	    );

		$this->localization_domain = CHILD_THEME_NAME;
		$this->option_name = 'iwebsite_discount_items_per_order_for_same_category';
		$this->settings = $this->get_options();

		add_action( 'woocommerce_cart_calculate_fees' , array( $this , 'one_category_product_sale' ) );
	}

	public function one_category_product_sale(){
		global $woocommerce;
		
		$discount_product 		= array();
		$cart_items 			= $woocommerce->cart->get_cart();
		$j 						= 0;
		$count_products			= 0;
		$ar_discount_products	= array();
		$total_discount 		= 0;
		$iwebsite_all_products	= get_option( 'iwebsite_all_products' );
		$discount_ahuz 			= false;
		if ( $iwebsite_all_products ){
			if ( isset( $iwebsite_all_products['active_sale'] ) && $iwebsite_all_products['active_sale'] ){
				$discount_ahuz  = $iwebsite_all_products['discount_value'];
			} 
		}

	
		if ( $discount_ahuz ){
			$global_discount = $discount_ahuz / 100; 
		} else {
			$global_discount = false;
		}
		// show( $global_discount, 'global_discount' );

		if( isset( $this->settings  ) && is_array( $this->settings ) ){

			foreach ( $this->settings as $sale_key => $sale  ){
				$total_sum 				= 0;	
				// $total_discount 		= 0;
				$product_in_cart 		= array();
				$sales_categories 		= ( isset( $sale['sales_categories'] ) ) 	? $sale['sales_categories'] 			: false;
				$quantity_products 		= ( isset( $sale['quantity_products'] ) ) 	? $sale['quantity_products'] 			: false;
				$price_for_products 	= ( isset( $sale['price_for_products'] ) ) 	? intval( $sale['price_for_products'] ) : false;
				$this->discount_label 	= isset( $sale[ 'discount_explanation' ] )? $sale[ 'discount_explanation' ] : '';
				if ( $this->discount_label === '' ){
					$this->discount_label = __( 'Cart discount', IWEBSITE_SALE_NAME );
				}

				if ( !$sales_categories ) continue;

				foreach ( $cart_items as $cart_item_key => $cart_item ) {
					$product_id = $cart_item['product_id'];
					$cart_item_categories = wp_get_post_terms( $product_id, 'product_cat' );
					$category_id = false;

					foreach ( $cart_item_categories as $item_key => $item_category ) {
							
						if ( in_array( $item_category->term_id,  $sales_categories ) ){
							$category_id = $item_category->term_id;
							break;
						}
					}
			
					if( $product_id && $category_id != false ){			
						$variation_id 		= $cart_item['variation_id'];	
						$variable_product 	= new WC_Product_Variation( $variation_id );
						$regular_price 		= $variable_product->get_price();
						$sales_price 		= $variable_product->get_sale_price();	
						$price = ( $sales_price )? $sales_price : $regular_price;
						if ( $global_discount != false ){
							$price 			= $price - $price * $global_discount;
						}
						$quantity 			= $cart_item['quantity']; // count same items 
						$count_products += $quantity; // count of elements in cart
						if ( $quantity > 1 ){
							while( $quantity ){ // Quantity need for number of same products

								$product_in_cart[$category_id][$j]['price'] 		= $price;
								$product_in_cart[$category_id][$j]['product_id'] 	= $product_id;
								$quantity--;
								$j++;
							}		
						} else {
							$product_in_cart[$category_id][$j]['price'] 		= $price;
							$product_in_cart[$category_id][$j]['product_id'] 	= $product_id;
							$j++;
						}
					}

				}
				$category_id = false;


				if( isset( $product_in_cart ) && is_array( $product_in_cart ) ){
					foreach ($product_in_cart as $key => $value) {	
						usort( $value, function($a, $b){
							return $a['price'] - $b['price'];
						});
						if ( !in_array( $value , $product_in_cart ) )
							$product_in_cart[$key] = $value;
					}
				}				

				$multiplicator = 1;
				foreach ($product_in_cart as $category_key => $products ) {
				//	$count_products 	= count( $products );
					$temp_multiplicator = floor( $count_products / $quantity_products );
					if ( $temp_multiplicator > 1 ){
						$multiplicator = $temp_multiplicator;
					}

					if ( $count_products < $quantity_products ) break;
					$module = $count_products % $quantity_products;
					if ( $module != 0  ){
						while ( $module > 0 ){
							array_pop( $products );
							$module--;
							$count_products--;
						}
					} 
					
					foreach ( $products as $product_key => $product ) {
						$total_sum += $product['price'];

					}
				}

				if ( $total_sum == 0 ) continue;
				// $total_discount += $price_for_products * $multiplicator;
				$total_discount = $total_sum - ( $price_for_products * $multiplicator );
				if (  $total_discount > 0 )
					$total_discount *= -1; // convert positive to negative fees
				$woocommerce->cart->add_fee( $this->discount_label , $total_discount, true, '' ); // add negative fees
			}
		}
		// show( $total_discount, '$total_discount' );
		return $total_discount;
	}

	// get discount settings for current tab in admin page 
    public function get_options() {
    	// show( $this->option_name, '$this->option_name' );
    	// delete_option( $this->option_name );
    	if ( !$options = get_option( $this->option_name ) ) {
	        $options[] = $this->default_settings ;
    		update_option( $this->option_name, $options );
    	}
    	// if empty value was saved
    	foreach ( $options as $key => $option ) {
    		$options[$key] = array_merge( $this->default_settings, $option );
    	}
    	// show( $options, 'optinos' );	
   		return $options;
    } 

	// 
	public function set_discount_settings( $new_settings ){
		$this->settings = $new_settings;
		return $this->settings;
	}


	public function admin_page_settings(){
		$i = 1;
		foreach ( $this->settings as $setting_key => $setting_value ) {
			$price 				= ( isset( $setting_value['price_for_products'] ) )? $setting_value['price_for_products'] :  false ;
			$active_sale 		= ( isset( $setting_value['active_sale'] ) )? $setting_value['active_sale'] : false ;
			$explanation 		= ( isset( $setting_value['discount_explanation'] ) ) ? $setting_value['discount_explanation'] : '';
			$sales_categories	= ( isset( $setting_value['sales_categories'] ) )? array_values( $setting_value['sales_categories'] ) : array() ;
			$quantity_products 	= ( isset( $setting_value['quantity_products'] ) )? $setting_value['quantity_products'] : 0 ;
			$categories_names 	= print_categories_names( $sales_categories );
?>
			<h3 class="section-title">
				<span><?php _e( 'Sale number', IWEBSITE_SALE_NAME ); ?><span class="number"><?php echo $i; ?></span></span>
                <?php if( $categories_names ){ ?><span class="cats"><?php echo $categories_names; ?></span><?php } ?>
                <span class="toggle-indicator" aria-hidden="true"></span>
            </h3>
			<div id="<?php echo $setting_key; ?>" class="sale-repeater">
				<div class="clearfix">
					<div class="section-discount-content">
						<?php iwebsite_active_sale( $setting_value, $setting_key ); ?>
						<?php iwebsite_choice_category_section( $setting_value, $setting_key ); ?>	
						<div>
							<div class="row-title dib">
								<strong><?php _e( 'Quantity of products from one category when sale work!', IWEBSITE_SALE_NAME ); ?></strong>
							</div>
							<div class="row-content dib">
								<label for="quantity_products">
									<input type="number" name="quantity_products[<?php echo $setting_key; ?>]" value="<?php echo $quantity_products; ?>"  >
								</label>
							</div>
						</div>
						<div>
							<div class="row-title dib">
								<strong><?php _e( 'Price for X products from one category', IWEBSITE_SALE_NAME ); ?></strong>
							</div>
							<div class="row-content dib">
								<label for="price_for_products">
									<input type="text" name="price_for_products[<?php echo $setting_key; ?>]" value="<?php echo $price; ?>"  >
								</label>
							</div>
						</div>	
						<div>
							<div class="row-title dib">
								<strong><?php _e( 'Enter label for explanation on cart page', IWEBSITE_SALE_NAME ); ?></strong>									
							</div>
							<div class="row-content dib">
								<label for="discount_explanation">
									<input type="text" name="discount_explanation[<?php echo $setting_key; ?>]" value="<?php echo $explanation; ?>"  >
								</label>
							</div>
						</div>
						<?php iwebsite_add_image( $setting_value, $setting_key ); ?>										
						<?php iwebsite_date_pickup( $setting_value, $setting_key ); ?>
					</div>
					<div class="section-discount-admin">
						<button class="sale-repeater-btn"><span class="label"><?php _e( 'Add row', IWEBSITE_SALE_NAME ); ?></span></button>
						<button class="delete-row"><span class="label"><?php _e( 'Delete row from repeater', IWEBSITE_SALE_NAME ) ?></span></button>
					</div>
				</div>
			</div>
<?php
		$i++;
		}
	}
}