<?php  
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
class iWebsite_Fixed_Price_For_Category {
	public $settings;
	public $has_user_role;
	public $role_discount_type_in_terms;
	public $role_in_discount_amount_in_terms;
	public $locale;
	public $total_discount_email;
	public $localization_domain;
	public $option_name;
	public $default_settings;
	public $another_discounts;
	
	public function __construct( $language = 'he' ){		
		
		$this->locale 				= '';
		$this->localization_domain 	= ( defined( 'ICL_LANGUAGE_CODE' ) )? ICL_LANGUAGE_CODE : '';
		$this->total_discount_email	= 0;
		$this->option_name 			= 'iwebsite_fixed_price_for_category';
		$this->default_settings 	= array( 
			'active_sale' 			=> '',
            'sales_categories' 	=>  array(),
			'discount_value'		=> '',
			'banner' 				=> '',
		);
		$this->settings				= $this->get_options();
		
		if( !is_admin() ){	
			$this->add_filters();	
		} 
	}
	public function add_filters(){
		
		add_action( 'woocommerce_before_calculate_totals', 	array( $this, 'change_price_in_object' ), 140, 1 );
		// filter to output html price in shop / product-category / single product
		add_filter( 'woocommerce_get_price_html',  			array( $this, 'change_html_sale_price' ), 111, 2 );

		// filter output subtotal price for each cart item on cart/checkout pages ( $price_product * $quantity )
		add_filter( 'woocommerce_cart_item_subtotal', 		array( $this, 'calculate_cart_item_subtotals' ), 111, 3 );

		// filter displaying price on minicart 
		add_filter( 'woocommerce_cart_item_price', 			array( $this, 'cart_item_price_html' ), 111, 3 );
		
		// add_action( 'custom_discount_cart_hook', 			array( $this, 'discount_label_to_product_in_cart'), 111 );
		add_action( 'woocommerce_email_after_order_table', 	array( $this, 'discount_in_email') );

	}

	public function calculate_cart_item_subtotals( $total_price, $cart_item, $cart_item_key  ){
		$product_id = $cart_item['product_id'];
		$sale_price = $this->get_sale_price_for_product( $product_id );
		if ( $sale_price ){
			$total_price = $sale_price * $cart_item['quantity'];
			$total_price = wc_price( $total_price );
	    }		
		return $total_price;
	}

	public function cart_item_price_html( $price, $cart_item, $cart_item_key ){
		$product_id = $cart_item['product_id'];
		$sale_price = $this->get_sale_price_for_product( $product_id );
		if ( $sale_price ) {
			$price = wc_price( $sale_price );
		}
		return $price;
	}

// For single product / category page
	public function change_html_sale_price( $price ){	

		global $product, $post;

		if ( !$product ){
			$product = wc_get_product( $post->ID );
		}
		if ( !$product ){
			return;
		}
		$product_id = $product->get_id();
		
		if ( $product->is_type( 'variable' ) ){
			$product_price = $product->get_variation_regular_price();
		} else { 
			$product_price = $product->get_regular_price();
		}
		$sale_price = $this->get_sale_price_for_product( $product_id );


		if ( $sale_price ) {
		    $price = '<del>'. wc_price( $product_price ) .'</del>';
		    $price .= '<ins>'. wc_price( $sale_price ) .'</ins>';
		}
		return $price;
	}

// Get discount settings for product category by product object / product id
	public function get_sale_price_for_product( $product  ){
		if ( is_numeric( $product ) ){
			$product_id = $product;
		} else {
			if ( $product->is_type( 'simple' ) ){
				$product_id =  $product->get_id(); 
			} else { 
				$product_id =  $product->get_parent_id(); 
			}
		}
		
		$price 					= $this->get_product_price_by_id( $product );

		// $additional_discount 	= $this->get_user_role_additional_discount( $product_id );
		// if ( $additional_discount && $additional_discount != $price ){
		// 	$price 				= $additional_discount;
		// } 

		$sale_price 		= false;
		$product_categories = get_the_terms( $product_id, 'product_cat' );
		if ( $product_categories ){
			foreach ( $product_categories as $key => $product_cat ) {
				$product_category[] = $product_cat->term_id;
			}
			$sale_repeater = $this->settings;
			$settings = false;
			if ( $sale_repeater ){
				foreach( $sale_repeater as $repeater ){
					$discount_categories 	= ( isset( $repeater['sales_categories'] ) && !empty( $repeater['sales_categories'] ) )?  $repeater['sales_categories'] : false ;
					$discount 				= ( isset( $repeater['discount_value'] ) && !empty( $repeater['discount_value'] ) ) ? $repeater['discount_value'] : false ;
					$active_sale 			= ( isset( $repeater['active_sale'] ) ) ? $repeater['active_sale'] : false ;
				
					if ( !$active_sale || !$discount_categories || !$discount ){
						continue;
					}
					
					foreach ( $product_categories as $key => $product_cat ) {	
						$cat_id = $product_cat->term_id;

						if ( in_array( $cat_id , $discount_categories ) ){
							$settings['discount'] = $discount;
							break;
						}
					}
				}
			}		

			if ( is_array( $settings ) && !empty( $settings ) && $settings['discount'] ){	
				$sale_price 		= $settings['discount'];;
			}
		}
		return $sale_price;
	}

	public function get_product_price_by_id( $product ){
		if ( is_numeric( $product ) ){
			$product = wc_get_product( $product );
		} 
		$product_id = $product->get_id();
		$price = false;
		if ( $product ){
			if ( $product->is_type( 'variable' ) ){
				$price = ( $product->get_variation_sale_price() )? $product->get_variation_sale_price() : $product->get_variation_regular_price() ;
			} else { 
				$price = ( $product->get_sale_price() ) ? $product->get_sale_price() : $product->get_regular_price();
			}
		}

		return $price;
	}



	// new funciton 
	// Change total and subtotal value at checkout/cart pages on cart item level 
	public function change_price_in_object( $cart ) {
		//  Exit function if price is changed at backend
		if ( is_admin() && ! defined( 'DOING_AJAX' ) )
			return;
		foreach ( $cart->get_cart() as $key => $item ) {
			$discount_amount		= 0;
			$variation_id 			= $item['data']->get_id();
			$product_id 			= wp_get_post_parent_id( $variation_id );
			$product 				= wc_get_product( $product_id );
			
			if( !isset( $product ) || !$product ) 
				continue;
			$product_price 			= $product->get_price();
		
			$price_after_discount 	= intval( $this->get_sale_price_for_product( $product_id ) );
			$user_discount 			= $this->get_user_role_additional_discount( $product_id );
	
			if ( $price_after_discount ){

				$total 	= $price_after_discount; 
				$item['data']->set_price( ( float ) $total );
			}
		}
	} 


// new code

	// Output admin subpage with settings form 
	public function admin_page_settings(){
		$i = 1;
		foreach ( $this->settings as $setting_key => $setting_value ) {
			$sale_end 			= ( isset( $setting_value['sale_end'] ) )? $setting_value['sale_end'] : date("y-m-d") ;
			$sale_start 		= ( isset( $setting_value['sale_start'] ) )? $setting_value['sale_start'] : date("y-m-d") ;
			$discount_value 	= ( isset( $setting_value['discount_value'] ) )? $setting_value['discount_value'] : '';
			$sales_categories 	= ( isset( $setting_value['sales_categories'] ) )? $setting_value['sales_categories'] : array() ;
			$discount_measure 	= ( isset( $setting_value['discount_measure'] ) ) ? $setting_value['discount_measure'] : 'percent' ;
			$categories_names 	= print_categories_names( $sales_categories );
?>		
			<h3 class="section-title">
				<?php _e( 'Sale number', $this->localization_domain ); ?><span class="number"><?php echo $i; ?></span>
				<?php if( $categories_names ){ ?><span class="cats"><?php echo $categories_names; ?></span><?php } ?>
				<span class="toggle-indicator" aria-hidden="true"></span>	
			</h3>
			<div id="<?php echo $setting_key ?>" class="sale-repeater">	
				<div class="clearfix">
					<div class="section-discount-content">
						<?php iwebsite_active_sale( $setting_value, $setting_key );  ?>						
						<?php iwebsite_choice_category_section( $setting_value, $setting_key ); ?>				
						<div class="discount-value">
							<div class="row-title dib">	
								<strong><?php _e( 'Discount value', IWEBSITE_SALE_NAME ); ?></strong>
							</div>
							<div class="row-content dib">
								<label for="discount_value">
									<input type="text" name="discount_value[<?php echo $setting_key; ?>]" value="<?php echo $discount_value ?>" >
								</label>
							</div>
						</div>
						<?php iwebsite_add_image( $setting_value, $setting_key ); ?>
					</div>
					<div class="section-discount-admin">
						<button class="sale-repeater-btn">
							<span class="label"><?php _e( 'Add row', IWEBSITE_SALE_NAME ); ?></span>
						</button>
						<button class="delete-row">
							<span class="label"><?php _e( 'Delete row from repeater', IWEBSITE_SALE_NAME ) ?></span>
						</button>
					</div>
				</div>
			</div>	
	<?php
		$i++;	
		}		
	}


	// get discount settings for current tab in admin page 
    public function get_options() {
    	if ( !$options = get_option( $this->option_name ) ) {
	        $options[] = $this->default_settings ;
    		update_option( $this->option_name, $options );
    	}
    	// if empty value was saved
    	foreach ( $options as $key => $option ) {
    		$options[$key] = array_merge( $this->default_settings, $option );
    	}

   		return $options;
    } 


	public function set_discount_settings( $new_settings ){
		$this->settings = $new_settings;
		return $this->settings;
	}

	
// New methods for interaction with another discounts
	// get userrole discount
	public function get_user_role_additional_discount( $product_id  ){
    	$current_user 		= wp_get_current_user();
		$current_user_roles = $current_user->roles;
		$price 				= $this->get_product_price_by_id( $product_id );
		$sale_price 		= false;
		$ar_settings 		= get_option( 'iwebsite_sale_for_userrole'.$this->locale );
		if ( is_array( $current_user_roles ) ){
			$settings 		= false;
			if ( $ar_settings ){
				foreach( $ar_settings as $repeater ){
					// show( $repeater, 'repeater' );
					$active_sale = isset( $repeater['active_sale'] )? $repeater['active_sale'] : false;
					$userrole 	= $repeater['userrole'];
					$discount 	= ( isset( $repeater['discount_value'] ) && $repeater['discount_value'] != '' )? $repeater['discount_value'] : false;
					if ( !$discount || !$active_sale ) continue;
					$measure 	= ( isset( $repeater['discount_measure'] ) && $repeater['discount_measure'] != '' ) ? $repeater['discount_measure'] : 'percent' ;
					if ( in_array( $userrole, $current_user_roles ) ){
						$settings['discount'] = $discount;
						$settings['measure'] = $measure;
						break;
					}
				}
			}
			if ( is_array( $settings ) && !empty( $settings ) ){
				$discount_measure	= $settings['measure'];
				$discount			= $settings['discount'];
				$discount_price 	= ( $discount_measure == 'percent' )? ( $price * $discount / 100 ) : $discount;
				$sale_price 		= round( $price - $discount_price );
				// $sale_price 		= $discount_price;
			}
		}
		// show( $sale_price, 'sale_price for administrator' );
		return $sale_price;
	}




	public function get_additional_sale_price_for_product_by_cat( $product  ){
		if ( is_numeric( $product ) ){
			$product_id = $product;
		} else {
			if ( $product->is_type( 'simple' ) ){
				$product_id =  $product->get_id(); 
			} else { 
				$product_id =  $product->get_parent_id(); 
			}
		}
		$price 					= $this->get_product_price_by_id( $product );
		$userrole_price 		= $this->get_user_role_additional_discount( $product_id );
		if ( $userrole_price ){
			$price -= $userrole_price;
		}
		// show( $price, '$price from userrole discount' );
		$sale_price 		= false;
		$product_categories = get_the_terms( $product_id, 'product_cat' );
		if ( $product_categories ){
			foreach ( $product_categories as $key => $product_cat ) {
				$product_category[] = $product_cat->term_id;
			}
			$sale_repeater = get_option( 'iwebsite_discount_categories' );
			$settings = false;
			if ( $sale_repeater ){
				foreach( $sale_repeater as $repeater ){
					$discount_categories 	= ( isset( $repeater['sales_categories'] ) && !empty( $repeater['sales_categories'] ) )?  $repeater['sales_categories'] : false ;
					$discount 				= ( isset( $repeater['discount_value'] ) && !empty( $repeater['discount_value'] ) ) ? $repeater['discount_value'] : false ;
					$measure 				= ( isset( $repeater['discount_measure'] ) ) ? $repeater['discount_measure'] : 'percent' ;
					$active_sale 			= ( isset( $repeater['active_sale'] ) ) ? $repeater['active_sale'] : false ;
					
					if ( !$active_sale || !$discount_categories || !$discount ){
						continue;
					}
					foreach ( $product_categories as $key => $product_cat ) {	
						$cat_id = $product_cat->term_id;

						if ( in_array( $cat_id , $discount_categories ) ){
							$settings['discount'] = $discount;
							$settings['measure'] = $measure;
							break;
						}
					}
				}
			}		
			// show( $settings, 'settings');
			if ( is_array( $settings ) && !empty( $settings ) ){
				$discount 			= $settings['discount'];
				$discount_measure 	= $settings['measure'];
				$discount_price 	= ( $discount_measure == 'percent' )? ( $price * $discount / 100 ) : $discount;
				$sale_price 		= ceil( $price - intval( $discount_price ) );

			}
		}
		return $sale_price;
	}



}
new iWebsite_Fixed_Price_For_Category();