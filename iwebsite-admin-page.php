<?php
if ( !class_exists( 'iWebsite_Admin_Page' ) ){
	class iWebsite_Admin_Page {

		// @string url to sale modle 
		public $path_to_files;

		// @string url for get param
		public $admin_settings_url;

		public $discount_settings;

		public $discount_pages;

		public $localization_domain;

		public function __construct( $discount_settings ){

		 	// $this->path_to_files 		= get_stylesheet_directory_uri().'/classes/woocommerce/class-iwebsite-sale'; 
		 	$this->path_to_files 		= plugin_dir_url( __FILE__ ); 
		 	$this->admin_settings_url 	= 'iwebsite-discounts-settings';
		 	// $this->admin_settings_url 	= 'userroles-discount';

			// Admin useful function - save discount settings, send message, get options, date pickup
		 	require_once( IWEBSITE_SALES_PATH .'/admin-functions/admin-functions.php');
			// include scripts and style for admin 
			add_action( 'admin_menu', 				array( $this, 'register_settings_page' ), 10 );
			add_action( 'admin_enqueue_scripts', 	array( $this, 'admin_iwebsite_sales_script' ), 50 );
			

			if ( isset( $discount_settings ) ){
				// show( $discount_settings, 'discount_settings' );
				$this->discount_settings = $discount_settings;
				$this->discount_pages = $this->get_discount_pages( $discount_settings );
			}
		}

		public function admin_iwebsite_sales_script(){	
			if ( isset( $_GET['page'] ) && in_array( $_GET['page'], $this->discount_pages ) ){
				wp_enqueue_script( 'jquery-ui-core' );
				wp_enqueue_script( 'jquery-ui-accordion' );
				wp_enqueue_script( 'jquery-ui-tooltip' );
				// wp_enqueue_style( 'jquery-ui' 			, $this->path_to_files . '/admin/style/jquery-ui.min.css', '', IWEBSITE_SALE_VERSION );
				wp_enqueue_style( 'admin-sales-style'	, $this->path_to_files . '/admin/style/style.css', '', IWEBSITE_SALE_VERSION  );
				wp_enqueue_script( 'admin-sales-script'	, $this->path_to_files . '/admin/js/script.js', array( 'jquery', 'jquery-ui-accordion' ), '',true ); 
			}
		}

		/**
		 * Register the menu item and its sub menu's.
		 *
		 * @global array $submenu used to change the label on the first item.
		 */
		public function register_settings_page() {
			$icon_svg 			= '';
			$manage_options_cap = 'manage_options';
			// Add main page.
			$admin_page = add_menu_page( 'Discount: General Settings', 'iWebsite Discount', $manage_options_cap, $this->admin_settings_url , array( $this,'load_page' ), $icon_svg, '50' );
			// Sub menu pages.
			foreach( $this->discount_settings as $discount ){
				if( $discount['status'] ){
					$submenu_pages [] = array(
						'iwebsite-discounts-settings', // main-menu
						$icon_svg,
						'<span>' . $discount['label']. '</span>',
						$manage_options_cap,
						$discount['type'], // submenu
						array( $this, 'load_page' ),
					);
				}
			}

			// return;
			// Allow submenu pages manipulation.
			$submenu_pages = apply_filters( 'wpseo_submenu_pages', $submenu_pages );
			// show( $submenu_pages, 'submenu_pages' );
			// Loop through submenu pages and add them pages.
			if ( count( $submenu_pages ) ) {
				foreach ( $submenu_pages as $submenu_page ) {		
					$admin_page = add_submenu_page( $submenu_page[0], $submenu_page[2] . ' - iWebsite - discounts', $submenu_page[2], $submenu_page[3], $submenu_page[4], $submenu_page[5] );
					// show( $admin_page, '$admin_page' );
					if ( isset( $submenu_page[6] ) && ( is_array( $submenu_page[6] ) && $submenu_page[6] !== array() ) ) {
						foreach ( $submenu_page[6] as $submenu_page_action ) {
							add_action( 'load-' . $admin_page, $submenu_page_action );
						}
					}
				}
			}
			// global $submenu;
			// if ( isset( $submenu['iwebsite-discounts-settings'] ) && current_user_can( $manage_options_cap ) ) {
			// 	$submenu['iwebsite-discounts-settings'][0][0] = __( 'General', 'iwebsite-discounts' );
			// }
		}	

		public function load_page() {
			// from discount pages sale file
			$class_name = $this->get_discount_class_name();
			if ( !$class_name ){
				$this->discounts_description();
			} else {
				$sale = new $class_name( array() );
				// $this->iwebsite_discount_update_message( $sale );
				$this->iwebsite_sales_settings_submission( $sale );
?>
				<div class="wrap">
					<form action="#" method="post" id="discount-settings-form">
						<div class="sale-container-inner">
							<?php $sale->admin_page_settings(); ?>
						</div>
						<div class="submit">
							<input type="submit" name="sales_settings" value="<?php _e( 'Save settings', IWEBSITE_SALE_NAME ); ?>" class="button button-primary">
							<?php wp_nonce_field( 'sales-settings-update' ); ?>
						</div>
					</form>
				</div>
				<?php
			// === FOR ADMIN SETTINGS === //
			}
		}

		private function get_discount_pages( $settings ){
			$pages = array();
			if ( is_array( $settings ) ){
				foreach ( $settings as $key => $setting ) {
					$pages[] = $setting['type'];
				}
			}
			return $pages;
		}

		private function get_discount_class_name(){
			$discount_type 	= $_GET['page'];
			if ( $discount_type  == 'iwebsite-discounts-settings' ){
				return false;
			} else {
				$class_name = '';
				foreach ( $this->discount_settings as $key => $value ) {
					if ( $discount_type == $value['type'] ){
						$class_name = $value['class'];
						break;
					}	
				}
				return $class_name;
			}
		}


	    private function iwebsite_discount_update_message( $sale ){
      		$message = null;
      		if ( isset( $_POST['sales_settings'] ) && $_POST['sales_settings'] ) {
				if ( $this->iwebsite_sales_settings_submission( $sale ) ) {
					$message = '<div class="updated"><p>' . __( 'Success! Your changes were sucessfully saved!', IWEBSITE_SALE_NAME ) . '</p></div>';
				} else{
					$message = '<div class="error"><p>' . __( 'Error. Either you did not make any changes, or your changes did not save. Please try again.', IWEBSITE_SALE_NAME ) . '</p></div>';
				}
      		}
      		// show( $message, 'message' );
			return $message;
	    }

	    // Saving user settings, work with POST variable, updating variables with settings
	    public function iwebsite_sales_settings_submission( $sale ) {
	    	if ( isset( $_POST['sales_settings'] ) && $_POST['sales_settings'] ) {
				if ( !wp_verify_nonce( $_POST['_wpnonce'], 'sales-settings-update' ) ) {
					die( __('Whoops! There was a problem with the data you posted. Please go back and try again.', IWEBSITE_SALE_NAME ) ); 
				}
					
		        if ( isset( $_POST ) ) {
		        	// $settings				= $sale->settings;
					// $ar_settings 			= $settings[0];
					$ar_temp_settings		= array();
					$ar_settings 			= $this->get_settings_example_from_array( $sale->settings );
					$current_discount_type 	= $sale->option_name;
					if ( !isset( $_POST['sales_categories'] ) ){
						$_POST['sales_categories'] = array();
					}
			
					foreach ( $_POST as $post_key => $post_value ) {
						// if ( isset( $_GET['test'] ) ){
							// show( is_array( $post_value ) , 'is array' );
							// show( !intval( $post_key ), 'NOT INTVAL' );
							// show( $sale->option_name  , 'SALE iwebsite_all_products option ' );
						// }

						if ( array_key_exists ( $post_key, $ar_settings ) ){

							if ( is_array( $post_value ) ){
								// if ( $post_key == 'sales_categories' && count( $post_value ) == 0 ){
								// 	$post_value = array( 0 );
								// }

								foreach ( $post_value as $key => $val ) {
									// if ( $val == ''  ) continue;
									if ( $val == ''  ) $val = false;
									$ar_temp_settings[$key][$post_key] = $val;  
								}
							} else {
								if ( !intval( $post_key ) && $sale->option_name != 'iwebsite_all_products' ) continue; 
								if (  $post_value == '' )  $post_value = false;
								$ar_temp_settings[$post_key] = $post_value; 
							}
						} else {
							if ( $post_key == 'sale_permanent' ){
								if ( is_array( $post_value ) ){
									foreach ( $post_value as $key => $val ) {
										$ar_temp_settings[$key][$post_key] = $val; 
									}
								}
							}
						}
						// show( $key, '$key' );
						if ( !isset( $_POST['all_product_settings'] )  ){
							$ar_temp_settings[$key] = array_merge( $sale->default_settings,  $ar_temp_settings[$key] );
						} else {
							$ar_temp_settings = array_merge( $sale->default_settings,  $ar_temp_settings);
						}
					}
					if ( isset( $_GET['test'] ) ){
						// show( $ar_settings, 'ar_settings' );
						// show( $current_discount_type, 'current_discount_type' );
						// show( $ar_temp_settings, ' Settings to save' );
						return;
					}
					// pass saved settings to class to update in on a page 
					$sale->set_discount_settings( $ar_temp_settings );
					update_option( $current_discount_type, $ar_temp_settings );
					
					if ( isset( $_GET ) && $_GET == 'delete' ){
						delete_option( $current_discount_type );
					}
		        }
		    	return false; 
		    }	     
	    }

	    public function discounts_description(){
?>
			<div class="wrap">
			<?php
			if ( $this->discount_settings ){
				foreach ( $this->discount_settings as $key => $setting ) {
					$name 			= $setting['type'];
					$link 			= admin_url('admin.php?page=' . $name );
					$description 	= $setting['description'];
?>					
					<div>
						<h4><?php echo $name ?></h4>
						<p><?php echo $description; ?></p>
						<a href="<?php echo $link; ?>" class="button"><?php _e( 'Settings', IWEBSITE_SALE_NAME ); ?></a>
					</div>	
<?php
				}
			}
			?>
			</div>
<?php
	    }

		public function get_settings_example_from_array( $ar_settings ){
			$settings = false;
			if ( is_array( $ar_settings ) ){
				foreach ( $ar_settings as $key => $value ) {
					if ( is_numeric( $key ) ){
						if ( !empty( $value ) ){
							$settings = $value;
							break;
						}
					} else {
						$settings = $ar_settings;
						break;
					}
				}
			} else {
				$settings = $ar_settings;
			}
			return $settings;
		}


	}
}

