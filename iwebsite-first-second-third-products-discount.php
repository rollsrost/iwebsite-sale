<?php  

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class iWebsite_First_Second_Third_Product_Discount {
	public $sale_categories;
	public $discount_value;
	public $discount_product;
	public $total_discount;
	public $taxonomy;
	public $settings;
	public $default_settings;
	public $localization_domain;
	public $product_quantity;
	public $product_in_cart ;
	public $description_for_discount = array();

	public $option_name;

	public function __construct( ){
		add_action( 'woocommerce_cart_calculate_fees', array( $this,  'discount_complex' ) );
		add_action( 'woocommerce_widget_shopping_cart_before_buttons', array( $this, 'discount_for_minicart' ), 20 );

		$this->taxonomy 				= 'product_cat';
		$this->localization_domain 		= CHILD_THEME_NAME;
		$this->total_discount 			= 0;
		$this->cart_product_quantity 	= 0;

		$this->default_settings = array(
			'first_product_discount_label' 	=> '',
			'second_product_discount_label' => '',
			'third_product_discount_label' 	=> '',
			'active_sale' 					=> '',
			'third_product_sale' 			=> '',
			'first_product_discount_value' 	=> '',
			'second_product_discount_value' => '',
			'third_product_discount_value' 	=> '',	
			'sales_categories' =>  array(),
	        'sale_permanent' => true,
	        'sale_start' =>  '',
	        'sale_end' =>  '',
	        'banner' => ''
		);

		$this->option_name = 'iwebsite_discount_first_second_third_product';
		$this->settings = $this->get_options();
	}

	public function discount_complex(){

		global $woocommerce;
	
		$total_discount = $this->get_cart_total_with_discount();
		if( $total_discount ){
 			$total_discount *= -1; // convert positive to negative fees
			$woocommerce->cart->add_fee(  __( 'Discount', IWEBSITE_SALE_NAME ), $total_discount, true, '' ); // add negative fees	
			add_action( 'woocommerce_cart_collaterals', array( $this, 'additional_discount_explain' ),  5 );
		}
		return $total_discount;
	}
	
	public function calculate_total_discount( $product_in_cart, $settings ){
		$sale 						= $this->get_sale_variable( $settings );
		$len 						= count( $product_in_cart );
		$total_discount 			= 0; 
		$count_discount_products 	= floor( $len/ $sale );
		switch ( $sale ) {
			case '3':
				$total_discount += $this->three_steps_discount( $product_in_cart, $settings );
				break;
			case '2' : 
				$total_discount += $this->two_steps_discount( $product_in_cart, $settings );
				break;
			case '1': 
				$total_discount += $this->one_steps_discount( $product_in_cart, $settings );
				break;
		}
		// if ( isset( $_GET['test'] ) ){ show( $total_discount, 'sale' ); }

		return $total_discount;
	}

	public function one_steps_discount( $product_in_cart, $settings ){
		$product_in_cart = array_values( $product_in_cart ); // Reset keys after remove elements form cart
		$first_product 		= array(
			'label' 	=> isset( $settings['first_product_discount_label'] )? $settings['first_product_discount_label'] : '',
			'discount' 	=> isset( $settings['first_product_discount_value' ] ) ? $settings['first_product_discount_value' ]/100 : false
		);
		$total_discount 			= 0;
		$description_for_discount 	= array();
		$product_cart_quantity = count( $product_in_cart ) - 1;

		for ( $i = 0 ; $i <= $product_cart_quantity; $i++ ) { 
			$first_product_key 	= $i;

			$first_discount 											 = $product_in_cart[$first_product_key]['price'] * $first_product['discount'];
			$description_for_discount[$first_product_key]['discount'] 	 = $first_product['discount'];
			$description_for_discount[$first_product_key]['label'] 		 = $first_product['label'];
			$description_for_discount[$first_product_key]['price'] 		 = $product_in_cart[$first_product_key]['price'];
			$description_for_discount[$first_product_key]['product_id'] = $product_in_cart[$first_product_key]['product_id'];

			$total_discount += $first_discount;
			
		}
		$this->description_for_discount = array_merge( $this->description_for_discount, $description_for_discount );
		return $total_discount;
	}

	public function two_steps_discount( $product_in_cart, $settings ){
		$product_in_cart 	= array_values( $product_in_cart ); // Reset keys after remove elements form cart
		$first_product 		= array(
			'label' 	=> isset( $settings['first_product_discount_label'] )? $settings['first_product_discount_label'] : '',
			'discount' 	=> isset( $settings['first_product_discount_value' ] ) ? $settings['first_product_discount_value' ]/100 : false
		);

		$second_product 	= array(
			'label' 	=> isset( $settings['second_product_discount_label'] )? $settings['second_product_discount_label'] : '',
			'discount'	=> isset( $settings['second_product_discount_value' ] )? $settings['second_product_discount_value' ]/100 : ''
		);

		$description_for_discount 	= array();
		$total_discount 			= 0;
		$sale 						= 2;
		$product_cart_quantity 		= count( $product_in_cart ) - 1;
		$len 						= count( $product_in_cart );
		$surplus					= 0;
		// show( $len, 'len' );
		if ( $len == 1 ){
			$surplus = 1;
		}
		for ( $i = 0 ; $i <= $product_cart_quantity; $i = $i + $sale ) {
			if ( $surplus == 1  ){
				$this->one_steps_discount( $product_in_cart, $settings );
			} else {
				$first_product_key 	= $i;
				$second_product_key = $first_product_key + 1;

				$first_discount 	= $product_in_cart[$first_product_key]['price'] * $second_product['discount'];
				$second_discount 	= $product_in_cart[$second_product_key]['price'] * $first_product['discount'];

				$description_for_discount[$first_product_key]['discount'] 	= $first_product['discount'];
				$description_for_discount[$second_product_key]['discount']	= $second_product['discount'];
				
				$description_for_discount[$first_product_key]['label'] 		= $first_product['label'];
				$description_for_discount[$second_product_key]['label']		= $second_product['label'];


				$description_for_discount[$first_product_key]['price'] 		= $product_in_cart[$second_product_key]['price'];
				$description_for_discount[$second_product_key]['price']		= $product_in_cart[$first_product_key]['price'];

				$description_for_discount[$first_product_key]['product_id'] 		= $product_in_cart[$second_product_key]['product_id'];
				$description_for_discount[$second_product_key]['product_id']		= $product_in_cart[$first_product_key]['product_id'];

				unset( $product_in_cart[$first_product_key] );
				unset( $product_in_cart[$second_product_key] );

				$total_discount += $first_discount + $second_discount;
			}
			$surplus = $len % $sale;
			
		}
		$this->description_for_discount = array_merge($this->description_for_discount, $description_for_discount );

		return $total_discount;
	}	

	public function three_steps_discount( $product_in_cart, $settings ){
		$first_product 		= array(
			'label' 	=> isset( $settings['first_product_discount_label'] )? $settings['first_product_discount_label'] : '',
			'discount' 	=> isset( $settings['first_product_discount_value' ] ) ? $settings['first_product_discount_value' ]/100 : false
		);

		$second_product 	= array(
			'label' 	=> isset( $settings['second_product_discount_label'] )? $settings['second_product_discount_label'] : '',
			'discount'	=> isset( $settings['second_product_discount_value' ] )? $settings['second_product_discount_value' ]/100 : ''
		);

		$third_product 	= array(
			'label' 	=> isset( $settings['third_product_discount_label'] )? $settings['third_product_discount_label'] : '',
			'discount'	=> isset( $settings['third_product_discount_value'] ) ? $settings['third_product_discount_value']/100 : ''
		);

		$description_for_discount 	= array();
		$total_discount 			= 0;
		$product_cart_quantity 		= count( $product_in_cart ) - 1;
		$len 						= count( $product_in_cart );
		$sale 						= 3;
		$surplus					= $len % $sale;
		for ( $i = 0 ; $i <= $product_cart_quantity; $i = $i + $sale ) { 
			if ( $len < 3 && $surplus == 2  ){
				$total_discount += $this->two_steps_discount( $product_in_cart, $settings );
			} elseif ( $len < 3 && $surplus == 1 ) {
				$total_discount += $this->one_steps_discount( $product_in_cart, $settings  );
			} else {
				$first_product_key 	= $i;
				$second_product_key = $first_product_key + 1;
				$third_product_key 	= $first_product_key + 2;

				$first_discount 	= $product_in_cart[$first_product_key]['price'] * $third_product['discount'];
				$second_discount 	= $product_in_cart[$second_product_key]['price'] * $second_product['discount'];
				$third_discount 	= $product_in_cart[$third_product_key]['price'] * $first_product['discount'];

				$description_for_discount[$first_product_key]['discount'] 	= $first_product['discount'];
				$description_for_discount[$second_product_key]['discount']	= $second_product['discount'];
				$description_for_discount[$third_product_key]['discount'] 	= $third_product['discount'];

				$description_for_discount[$first_product_key]['label'] 		= $first_product['label'];
				$description_for_discount[$second_product_key]['label']		= $second_product['label'];
				$description_for_discount[$third_product_key]['label'] 		= $third_product['label'];

				$description_for_discount[$first_product_key]['price'] 		= $product_in_cart[$third_product_key]['price'];
				$description_for_discount[$second_product_key]['price']		= $product_in_cart[$second_product_key]['price'];
				$description_for_discount[$third_product_key]['price'] 		= $product_in_cart[$first_product_key]['price'];

				$description_for_discount[$first_product_key]['product_id'] 		= $product_in_cart[$third_product_key]['product_id'];
				$description_for_discount[$second_product_key]['product_id']		= $product_in_cart[$second_product_key]['product_id'];
				$description_for_discount[$third_product_key]['product_id'] 		= $product_in_cart[$first_product_key]['product_id'];
				
				unset( $product_in_cart[$first_product_key] );
				unset( $product_in_cart[$second_product_key] );
				unset( $product_in_cart[$third_product_key] );

				$total_discount += $first_discount + $second_discount + $third_discount;
			}
			$len	 = $len - 3;
			$surplus = $len % $sale;
		}
		$this->description_for_discount = array_merge($this->description_for_discount, $description_for_discount );

		return $total_discount;
	}

	//  check if product is in sale
	public function check_if_product_on_sale( $product_id, $sales_settings ){
		$sales_settings 		= ( is_array( $sales_settings ) ) ? $sales_settings : false ;
		$product_sale_category 	= false;
		if( !$sales_settings || empty( $sales_settings ) ){
			return false;
		}	
		$product_categories		= wp_get_post_terms( $product_id, $this->taxonomy, array( 'fields' => 'ids' ) );
		foreach ( $sales_settings as $key => $settings ) {
			$sales_categories = $settings['sales_categories'];
			foreach ( $sales_categories as $sale_category ) {
				if ( in_array( $sale_category, $product_categories ) ){
					$product_sale_category = $key;
					break 2;
				}
			}
		}
		return $product_sale_category;
	}

	// get settings array array, include in array only active sales
	// return array with active sales
	public function get_active_sales_from_settings(){
		$ar_discount_settings = array();
		$i = 1;
		foreach ( $this->settings as $key => $settings ) {
			$setting_key 					= 'Discount';
			$sales_categories 				= ( isset( $settings['sales_categories'] ) )? $settings['sales_categories'] : false;
			$active_sale 					= ( isset( $settings['active_sale'] ) )? $settings['active_sale'] : false;
			// 
			if ( !$sales_categories || !$active_sale ){
				continue; 
			} else {
				$key_name 							= $setting_key . '_' . $i;
				$ar_discount_settings[ $key_name ] 	= $settings;
				$i++;
			}
		}
		return $ar_discount_settings;
	}
	// Get cart items and group them by discount settings
	// return array with cart items that on sale
	public function get_grouped_discount_products_from_cart( $settings ){
		global $woocommerce;
		$items 						= $woocommerce->cart->get_cart();
		$product_in_cart 			= array();
		$sum_quantity 				= 0; 
		// additional discount
		// $discount_percentage		= get_field( 'discount_ahuz', 'option' );
		$iwebsite_all_products	= get_option( 'iwebsite_all_products' );
		$discount_percentage 			= false;
		if ( $iwebsite_all_products ){
			if ( isset( $iwebsite_all_products['active_sale'] ) && $iwebsite_all_products['active_sale'] ){
				$discount_percentage  = $iwebsite_all_products['discount_value'];
			} 
		}
		$global_discount 			= ( $discount_percentage ) ? $discount_percentage / 100 : false;
		foreach( $items as $item => $values ) { 
			$product_id 					= $values['product_id'];		
			$product_on_sale 				= $this->check_if_product_on_sale( $product_id, $settings );	
			if( $product_on_sale ){			
				$variation_id 				= $values['variation_id'];	
				// count same items 
				$quantity 					= $values['quantity'];
				$variable_product 			= new WC_Product_Variation( $variation_id );
				$regular_price 				= $variable_product->get_regular_price();	
				$sales_price 				= $variable_product->get_sale_price();

				// count of elements in cart
				$sum_quantity += $quantity;

				if ( $global_discount != false ){
					$regular_price 	= $regular_price - $regular_price * $global_discount;
					$sales_price 	= $sales_price - $sales_price * $global_discount;
				}		
				while( $quantity ){ // Quantity need for number of same products
					$price 								 = ( $sales_price )? $sales_price : $regular_price;
					$temp_array['price'] 				 = $price;
					$temp_array['product_id'] 			 = $product_id;
					$product_in_cart[$product_on_sale][] = $temp_array;
					$quantity--;
				}		
			}
		}
		$this->cart_product_quantity = $sum_quantity;
		return $product_in_cart; 	
	}

	public function get_sale_variable( $settings ){
		$sale = 1;
		if ( isset( $settings['third_product_discount_value'] ) && $settings['third_product_discount_value'] > 0 ){
			$sale = 3;
		} elseif ( isset( $settings['second_product_discount_value'] ) && $settings['second_product_discount_value'] > 0 ) {
			$sale = 2;
		} else {
			$sale = 1;
		}
		return $sale;
	}

	public function additional_discount_explain( ){
		global $woocommerce;
		// return;

		if (  $this->description_for_discount == null ||  $this->description_for_discount == '' ) return;
		$currency = get_woocommerce_currency_symbol();
		$discount_value = $this->discount_value;  
		echo '<h2>'.__( 'discount will work at cart page', IWEBSITE_SALE_NAME ).'</h2>';
		$i = 1;
		
		$length = sizeof( $this->description_for_discount );
		
		foreach( $this->description_for_discount as $key => $value ){
			if( isset( $value[ 'quantity' ] ) ){
				$product_quantity = $value[ 'quantity' ];
			} else {
				$product_quantity = 1;
			}
			if( isset($value[ 'label' ]) ){
				$product_label = $value[ 'label' ];	
			} else {
				$product_label = '';
			}

			if( isset($value[ 'product_id' ]) ){
				$product_id = $value[ 'product_id' ];
			}
			if( isset($value[ 'price' ]) ){
				$product_price = (int)$value[ 'price' ] * $product_quantity;
			}
			if( isset( $value[ 'discount' ] ) ){
				$product_discount = round( $product_price * ( $value[ 'discount' ] * $product_quantity ) );
				$discount_percentage = 100 * $value[ 'discount' ];
			} else {
				$product_discount = 0;
				$discount_percentage = '';
			}

			if ( $product_discount == 0 ) continue;
			$product = wc_get_product( $product_id );
			$product_title = $product->get_title();
			
			$price_after_discount = $product_price - $product_discount;
			// $discount = $product_price - $price_after_discount;

			$class = ( $i % 2 == 0 ) ? 'even' : 'odd';
			$last = ( $i  == $length )? 'last' : '';
			
			if( !isset( $product_discount ) && $product_discount == 0 ) return;
			?>
			<div class="clearfix sale <?php echo $class.' '.$last; ?>">
				<h4><?php echo $product_title; ?></h4>
	 			<div class="one-third first">
					<h5><?php echo $product_label; ?></h5>
				</div> 
				<div class="one-third">
					<p><?php _e( 'Discount value' , IWEBSITE_SALE_NAME );?></p><span>- <?php echo $product_discount; ?></span><span><?php echo $currency; ?></span>
				</div>
				<div class="one-third">
					<p><?php _e( 'Price after discount', $this->localization_domain ); ?></p> <span> <?php echo $price_after_discount.' ' . $currency; ?></span>
				</div>
			</div>
			<?php
				$i++;
		}
	}
	public function get_cart_total_with_discount(){
		global $woocommerce;
		$ar_product 			= array();
		$total_discount 		= 0;
		// get active sale settings
		$ar_discount_settings 	= $this->get_active_sales_from_settings(); 
		// get array sale cart items that grouped by discount settings
		$product_in_cart 		= $this->get_grouped_discount_products_from_cart( $ar_discount_settings );		

		if ( !is_array( $product_in_cart ) || $product_in_cart == null ){
			return false;
		}
		
		// sort 2 dimensional products array by price
		foreach ( $product_in_cart as $key => $products ) {
			usort( $product_in_cart[$key], function($a, $b){
				return $a['price'] - $b['price'];
			});
		}

		foreach ( $product_in_cart as $key => $product_in_cart ) {
			$settings 			= $ar_discount_settings[ $key ];
			$total_discount 	+= $this->calculate_total_discount(  $product_in_cart, $settings );
 		}
 		// $total_discount *= -1; // convert positive to negative fees
 		
 		if ( $total_discount == 0 ){
 			return false;
 		} else {
 			$this->total_discount = $total_discount;
			return $total_discount;
		}
	}

	public function discount_for_minicart(){
		global $woocommerce;
		$cart_total 		= $woocommerce->cart->total;
		$total_discount 	= $this->get_cart_total_with_discount();

		if ( $total_discount != 0 ){
			echo '<p class="minicart-discount"><span>'. __( 'Discount:', IWEBSITE_SALE_NAME ). '</span><strong>-'. wc_price( $total_discount ).'</strong></p>';
			echo '<p class="minicart-after-discount"><span>'. __( 'Price after discount:', IWEBSITE_SALE_NAME ). '</span><strong>'. wc_price( $cart_total ).'</strong></p>';
		}
		
	}

	public function admin_page_settings(){
		$i = 1;
		foreach ( $this->settings as $setting_key => $setting_value ) {
			$sale_end 						= ( isset( $setting_value['sale_end'] ) )? $setting_value['sale_end'] : date("y-m-d") ;
			$sale_start 					= ( isset( $setting_value['sale_start'] ) )? $setting_value['sale_start'] : date("y-m-d") ;
			$sales_categories 				= ( isset( $setting_value['sales_categories'] ) )? $setting_value['sales_categories'] : array() ;
			$first_product_discount_label 	= ( isset( $setting_value['first_product_discount_label'] ) )?  $setting_value['first_product_discount_label']: '';  // Label 1
			$second_product_discount_label 	= ( isset( $setting_value['second_product_discount_label'] ) )?  $setting_value['second_product_discount_label']: ''; // Label 2
			$third_product_discount_label 	= ( isset( $setting_value['third_product_discount_label'] ) )?  $setting_value['third_product_discount_label']: ''; // Label 3
			$first_product 					= ( isset( $setting_value['first_product_discount_value'] ) )?  $setting_value['first_product_discount_value']: '';
			$second_product 				= ( isset( $setting_value['second_product_discount_value'] ) )?  $setting_value['second_product_discount_value']: '';
			$third_product 					= ( isset( $setting_value['third_product_discount_value'] ) )?  $setting_value['third_product_discount_value']: '';	
			$active_sale 					= ( isset( $setting_value['active_sale']) && $setting_value['active_sale'] ) ? true : false;
			$third_product_sale 			= ( isset( $setting_value['third_product_sale'] ) && $setting_value['third_product_sale'] ) ? true : false;
			$categories_names 				= print_categories_names( $sales_categories );
?>	
			<h3 class="section-title">
				<?php _e( 'Sale number', IWEBSITE_SALE_NAME ); ?><span class="number"><?php echo $i; ?></span>
				<?php if( $categories_names ){ ?><span class="cats"><?php echo $categories_names; ?></span><?php } ?>
				<span class="toggle-indicator" aria-hidden="true"></span>
			</h3>
			<div id="<?php echo $setting_key ?>" class="sale-repeater">
				<div class="clearfix">
					<div class="section-discount-content">	
						<?php iwebsite_choice_category_section( $setting_value, $setting_key ); ?>	
						<?php iwebsite_active_sale( $setting_value, $setting_key ); ?>
						<div>
							<div class="row-title dib">
								<strong><?php _e( 'First product sale label', IWEBSITE_SALE_NAME ); ?></strong>
							</div>
							<div class="row-content dib">
								<label for="first_product_discount_label">
									<input type="text" name="first_product_discount_label[<?php echo $setting_key; ?>]" value="<?php echo $first_product_discount_label; ?>"  >
								</label>
							</div>
						</div>
						<div>
							<div class="row-title dib">
								<strong><?php _e( 'First product sale discount', IWEBSITE_SALE_NAME ); ?></strong>
							</div>
							<div class="row-content dib">
								<label for="first_product_discount_value">
									<input type="text" name="first_product_discount_value[<?php echo $setting_key; ?>]" value="<?php echo $first_product; ?>"  >
								</label>
							</div>
						</div>				
						<div>
							<div class="row-title dib">
								<strong><?php _e( 'Second product sale label', IWEBSITE_SALE_NAME ); ?></strong>
							</div>
							<div class="row-content dib">
								<label for="second_product_discount_label">
									<input type="text" name="second_product_discount_label[<?php echo $setting_key; ?>]" value="<?php echo $second_product_discount_label; ?>"  >
								</label>
							</div>
						</div>
						<div>
							<div class="row-title dib">
								<strong><?php _e( 'Second product sale discount', IWEBSITE_SALE_NAME ); ?></strong>
							</div>
							<div class="row-content dib">
								<label for="second_product_discount_value">
									<input type="text" name="second_product_discount_value[<?php echo $setting_key; ?>]" value="<?php echo $second_product; ?>" >
								</label>
							</div>
						</div>
						<div>
							<div class="row-title dib">
								<strong><?php _e( 'Active third product sale', IWEBSITE_SALE_NAME ); ?></strong>
							</div>
							<div class="row-content dib">
								<label for="third_product_sale">
									<input type="checkbox" name="third_product_sale[<?php echo $setting_key; ?>]" value="true" <?php if ( $third_product_sale ) echo 'checked="cheked"'; ?> class="active-third-discount">
								</label>
							</div>
						</div>					
						<div class="third-product-fields" <?php if ( !$third_product_sale ) echo 'style="display:none"'; ?>>
							<div class="row-title dib">
								<strong><?php _e( 'Third product sale label', IWEBSITE_SALE_NAME ); ?></strong>
							</div>
							<div class="row-content dib">
								<label for="third_product_discount_label">
									<input type="text" name="third_product_discount_label[<?php echo $setting_key; ?>]" value="<?php echo $third_product_discount_label; ?>"  >
								</label>
							</div>
						</div>
						<div class="third-product-fields" <?php if ( !$third_product_sale ) echo 'style="display:none"'; ?>>
							<div class="row-title dib">
								<strong><?php _e( 'Third product sale discount', IWEBSITE_SALE_NAME ); ?></strong>
							</div>
							<div class="row-content dib">
								<label for="third_product_discount_value">
									<input type="text" name="third_product_discount_value[<?php echo $setting_key; ?>]" value="<?php echo $third_product; ?>"  >
								</label>
							</div>
						</div>
						<?php iwebsite_add_image( $setting_value, $setting_key ); ?>									
						<?php iwebsite_date_pickup( $setting_value, $setting_key );?>
					</div>
					<div class="section-discount-admin">
						<button class="sale-repeater-btn"><span class="label"><?php _e( 'Add row', IWEBSITE_SALE_NAME ); ?></span></button>
						<button class="delete-row"><span class="label"><?php _e( 'Delete row from repeater', IWEBSITE_SALE_NAME ) ?></span></button>
					</div>
				</div>
			</div>
		<?php
		$i++;	
		}
	}

	// get discount settings for current tab in admin page 
    public function get_options() {
    	// delete_option( $this->option_name ); 
    	if ( !$options = get_option( $this->option_name ) ) {
	        $options[] = $this->default_settings ;
    		update_option( $this->option_name, $options );
    	}
    	// if empty value was saved
    	foreach ( $options as $key => $option ) {
    		$options[$key] = array_merge( $this->default_settings, $option );
    	}

   		return $options;
    } 


	public function set_discount_settings( $new_settings ){
		$this->settings = $new_settings;
		return $this->settings;
	}

}
