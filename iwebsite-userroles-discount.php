<?php  

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// show( 'THIS IS IWEBSITE USER SALE ACTIVATE' );

class iWebsite_Discount_For_Userroles {
	// public $sale_categories;
	// public $discount_value;
	// public $discount_product;
	public $current_user_roles;
	public $settings = array();
	public $option_name;
	public $default_settings = array();


	public function __construct( ){
		$this->init_variables();

		// filter to output html price in shop / product-category / single product
		add_filter( 'woocommerce_get_price_html',  array( $this, 'change_html_sale_price' ), 90, 1 );

		// filter displaying price on minicart 
		add_filter( 'woocommerce_cart_item_price', array( $this, 'cart_item_price_html' ), 50, 3 );

		// change calculated subtotal on checkout page
		add_filter( 'woocommerce_cart_item_subtotal', array( $this, 'calculate_cart_item_subtotals' ), 95, 3 );

		// totals on cart page
		add_action( 'woocommerce_before_calculate_totals', array( $this, 'change_price_in_object' ), 99, 1 );

	}

	// calculate cart item subtotal on cart page
	// @return $total_price string
	public function calculate_cart_item_subtotals( $total_price, $cart_item, $cart_item_key  ){
		$product_id = $cart_item['product_id'];
		$sale_price = $this->get_sale_price_for_product( $product_id );
		if ( $sale_price ){
			$total_price = $sale_price * $cart_item['quantity'];
			$total_price = wc_price( $total_price );
	    }		
		return $total_price;
	}


	// get sale price for cart item element
	// return $price in html format ( string )
	public function cart_item_price_html( $price, $cart_item, $cart_item_key ){
		$product_id = $cart_item['product_id'];
		$sale_price = $this->get_sale_price_for_product( $product_id );
		if ( $sale_price ) {
			$price = wc_price( $sale_price );
		}
		return $price;
	}


	// For single product / category page
	// return $price in html format ( string )
	public function change_html_sale_price( $price ){	
		global $product, $post;
		if ( !$product ){
			$product = wc_get_product( $post->ID );
		}
		if ( !$product ){
			return $price;
		}

		$product_id = $product->get_id();
		if ( $product->is_type( 'variable' ) ){
			$product_price = $product->get_variation_regular_price();
		} else { 
			$product_price = $product->get_regular_price();
		}

		$sale_price = $this->get_sale_price_for_product( $product_id );
		if ( $sale_price ) {
		    $price = '<del>'.wc_price( $product_price ).'</del>';
		    $price .= '<ins>'.wc_price( $sale_price ).'</ins>';
		}

		return $price;
	}

	// Get discount settings for product category by product id
	// @return sale_price int 
	public function get_sale_price_for_product( $product_id  ){
		$price 				= $this->get_product_price_by_id( $product_id );
		$sale_price 		= false;
    	$current_user_roles = $this->current_user_roles;
		if ( is_array( $current_user_roles ) ){
			$settings 		= false;
			if ( $this->settings ){
				foreach( $this->settings as $repeater ){
					$active_sale = $repeater['active_sale'];
					$userrole 	= $repeater['userrole'];
					$discount 	= ( isset( $repeater['discount_value'] ) && $repeater['discount_value'] != '' )? $repeater['discount_value'] : false;
					if ( !$discount || !$active_sale ) continue;
					$measure 	= ( isset( $repeater['discount_measure'] ) && $repeater['discount_measure'] != '' ) ? $repeater['discount_measure'] : 'percent' ;
					if ( in_array( $userrole, $current_user_roles ) ){
						$settings['discount'] = $discount;
						$settings['measure'] = $measure;
						break;
					}
				}
			}
			if ( is_array( $settings ) && !empty( $settings ) ){
				$discount_measure	= $settings['measure'];
				$discount			= $settings['discount'];
				$discount_price 	= ( $discount_measure == 'percent' )? ( $price * $discount / 100 ) : $discount;
				$sale_price 		= round( $price - $discount_price );
			}
		}
		return $sale_price;
	}

	// detect product type and get it price by product id, if isset sale price - return sale price else regular
	// return $price int
	public function get_product_price_by_id( $product_id ){
		$product = wc_get_product( $product_id );
		$price = false;
		if ( $product ){
			if ( $product->is_type( 'variable' ) ){
				$price = ( $product->get_variation_sale_price() )? $product->get_variation_sale_price() : $price = $product->get_variation_regular_price();
			} else { 
				$price = ( $product->get_sale_price() ) ? $product->get_sale_price() : $product->get_regular_price();
			}
		}
		return $price;
	}


	public function change_price_in_object( $cart ) {
		//  Exit function if price is changed at backend
		if ( is_admin() && ! defined( 'DOING_AJAX' ) )
			return;

		foreach ( $cart->get_cart() as $key => $item ) {
			$variation_id 			= $item['data']->get_id();
			$product_id 			= wp_get_post_parent_id( $variation_id );
			$product 				= wc_get_product( $product_id );
			$discount_amount		= 0;
			if( !isset( $product ) ) 
				continue;
			$product_price 			= $product->get_price();
		
			$price_after_discount 	= $this->get_sale_price_for_product( $product_id );
			// $user_discount 			= $this->get_user_role_additional_discount( $product_price );
			
			if ( $price_after_discount ){				
				$discount_amount 	+= ( $product_price - $price_after_discount );
		    } else {
		    	continue;
				// $discount_amount 	+= $user_discount;
			}
			
			$total 	= $product_price - $discount_amount; 
			$item['data']->set_price( ( float ) $total );
		}
	}


################################################################

// Output admin subpage with settings form 
	public function admin_page_settings(){
	    global $wp_roles;
		$current_user 		= wp_get_current_user();
    	$user_roles 		= $current_user->roles;
		$wordpress_roles 	= $wp_roles->get_names();
		$i = 1;
		foreach ( $this->settings as $setting_key => $setting_value ) {
			$discount_value 	= ( isset( $setting_value['discount_value'] ) )? $setting_value['discount_value'] : '';
			$userrole 			= ( isset( $setting_value['userrole'] ) )? $setting_value['userrole'] : '' ;
			$sale_start 		= ( isset( $setting_value['sale_start'] ) )? $setting_value['sale_start'] : date("y-m-d") ;
			$sale_end			= ( isset( $setting_value['sale_end'] ) )? $setting_value['sale_end'] : date("y-m-d") ;
			$discount_measure 	= ( isset( $setting_value['discount_measure'] ) ) ? $setting_value['discount_measure'] : 'percent' ;
			$active_sale		= ( isset( $setting_value['active_sale']) && $setting_value['active_sale'] ) ? true : false;
?>		
			<h3 class="section-title">
				<?php _e( 'Sale number', IWEBSITE_SALE_NAME ); ?>
				<span class="number"><?php echo $i; ?></span>
				<span class="b"><?php echo sprintf( __( 'Discount for %s userrole', IWEBSITE_SALE_NAME ), $userrole );  ?></span>
				<span class="toggle-indicator" aria-hidden="true"></span>
			</h3>
			<div id="<?php echo $setting_key ?>" class="sale-repeater">	
				<div class="clearfix">
					<div class="section-discount-content">
						<div class="active-sale">
							<div class="row-title dib">
								<strong><?php _e( 'Active sale', IWEBSITE_SALE_NAME ); ?></strong>
							</div>
							<div class="row-content dib">
								<label for="first_product_discount_label">
									<input type="checkbox" name="active_sale[<?php echo $setting_key; ?>]" value="true" <?php if ( $active_sale ) echo 'checked="cheked"'; ?>>
								</label>
							</div>
						</div>					
						<div class="userrole-value">
							<div class="row-title dib">	
								<strong><?php _e( 'Select userrole', IWEBSITE_SALE_NAME ); ?></strong>
							</div>
							<div class="row-content dib">
								<select name="userrole[<?php echo $setting_key; ?>]">
									<!-- <option value=""></option> -->
									<?php foreach ( $wordpress_roles as $key => $role ) {
										$selected = ( $userrole == $key ) ? 'selected="selected"' : '';
										echo '<option value="'.$key.'" '.$selected.' >'.$role.'</option>';
									} ?>
								</select>
							</div>
						</div>
						<div class="discount-value">
							<div class="row-title dib">	
								<strong><?php _e( 'Discount value', IWEBSITE_SALE_NAME ); ?></strong>
							</div>
							<div class="row-content dib">
								<label for="discount_value">
									<input type="text" name="discount_value[<?php echo $setting_key; ?>]" value="<?php echo $discount_value ?>" >
								</label>
							</div>
						</div>
						<div class="discount-measure">
							<div class="row-title dib">
								<strong><?php _e( 'Discount measure', IWEBSITE_SALE_NAME ); ?></strong>
							</div>
							<div class="row-content dib">
								<label for="percent" class="b">
									<input type="radio" name="discount_measure[<?php echo $setting_key; ?>]" value="percent" id="percent" <?php if ( $discount_measure == 'percent' || $discount_measure == '' ) echo 'checked="checked"'; ?>>
									<span><?php _e( 'percent', IWEBSITE_SALE_NAME ) ?></span>
								</label>
								<label for="fixed" class="b">
									<input type="radio" name="discount_measure[<?php echo $setting_key; ?>]" value="fixed" id="fixed" <?php if ( $discount_measure == 'fixed' ) echo 'checked="checked"'; ?>>
									<span><?php _e( 'fixed', IWEBSITE_SALE_NAME) ?></span>
								</label>
							</div>
						</div>	
						<?php iwebsite_date_pickup( $setting_value, $setting_key ); ?>
					</div>
					<div class="section-discount-admin">
						<button class="sale-repeater-btn"><span class="label"><?php _e( 'Add row', IWEBSITE_SALE_NAME ); ?></span></button>
						<button class="delete-row"><span class="label"><?php _e( 'Delete row from repeater', IWEBSITE_SALE_NAME ) ?></span></button>
					</div>
				</div>
			</div>	
	<?php	
		$i++;
		}	
	}

	// get discount settings for current tab in admin page 
    public function get_options() {
    	if ( !$options = get_option( $this->option_name ) ) {
	        $options[] = $this->default_settings;
    		update_option( $this->option_name, $options );
    	}

    	foreach ( $options as $key => $option ) {
    		$options[$key] = array_merge( $this->default_settings, $option );
    	}
    	$this->settings = $options;
   		return $options;
    } 

    // setter for options
	public function set_discount_settings( $new_settings ){
		$this->settings = $new_settings;
		return $this->settings;
	}

	public function init_variables(){
    	$current_user = wp_get_current_user();
		$this->current_user_roles = $current_user->roles;

		$this->locale = ( defined( 'ICL_LANGUAGE_CODE' ) )? '_'. ICL_LANGUAGE_CODE : '';
		$this->option_name	= 'iwebsite_sale_for_userrole'.$this->locale;	
		$this->localization_domain = '';
		
		
		$this->default_settings = 	array(
			'active_sale' 		=> '',
	        'discount_value' 	=>  '',
	        'discount_measure' 	=> '',
	        'sale_permanent' 	=> true,
	        'sale_start' 		=>  '',
	        'sale_end' 			=>  '',
	        'userrole' 			=> '',
	    );

	    $this->settings = $this->get_options();
	}

}
?>