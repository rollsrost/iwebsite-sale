��    #      4  /   L           	               +     ;     T     ]  !   p  %   �  1   �     �      �          +     :  (   U  (   ~     �     �     �     �                ,     =     K  	   [     e  ,   n     �     �     �     �     �  n  �     ?     S  #   i     �     �     �     �  '   �  /   �  <   *	     g	  &   �	     �	     �	  &   �	  #   �	  ?   
     \
     |
  "   �
  &   �
  )   �
  
             2     L     \     r  ?     '   �     �     �     �                      
                                         "                                                       	               !             #                Active sale Add row Cart discount Choose category Delete row from repeater Discount Discount condition Discount for all products on site Discount for products from 1 category Discount for products from 1 category ( in cart ) Discount measure Discount order by some condition Discount text: Discount value Enable discount permanent? Enter label for explanation on cart page Fixed price for all products in category Image to frontend product: No image selected Product Category Discount Sale for all products Sale for selected category? Sale number Sales categories Save settings Select userrole Set image Settings Stepped discount for product from 1 category discount will work at cart page fixed no percent yes Project-Id-Version: iwebsite-sale
POT-Creation-Date: 2018-10-18 09:46+0200
PO-Revision-Date: 2018-10-18 18:01+0200
Last-Translator: 
Language-Team: 
Language: he_IL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Poedit-Basepath: ..
X-Poedit-SearchPathExcluded-0: *.js
X-Poedit-SearchPath-0: .
 הנחה פעילה להוסיף שורה הנחה על עגלת הקניות לבחור קטגוריה מחק שורה הנחה תנאי הנחה הנחה לכל המוצרים באתר הנחה למוצרים מקטגוריה אחת הנחה למוצרים מקטגוריה אחת (בעגלה) דיסקונט למדוד הנחה על פי תנאי כלשהו טקסט הנחה: ערך דיסקונט האם לאפשר הנחה קבועה? הסבר הנחה בדף תשלום מחיר קבוע עבור כל המוצרים בקטגוריה תמונה למוצר Frontend: לא נבחרה תמונה הנחה לקטגוריה מוצר הנחה  עבור כל המוצרים מכירה בקטגוריה שנבחרה? הנחה # קטגוריות הנחות שמירת שינויים בחר userrole הוספת תמונה הגדרות הנחה מדורגת עבור מוצר מקטגוריה אחת הנחות ינתנו בדף תשלום קבוע לא אחוזים כן 