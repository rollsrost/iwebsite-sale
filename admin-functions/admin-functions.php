<?php 

	// output date pickup block for temporary sales 
	function iwebsite_date_pickup( $settings, $key ){
		$today 			= date("y-m-d");
		$sale_end 		= ( isset( $settings['sale_end'] ) )? $settings['sale_end'] : $today ;
		$sale_start 	= ( isset( $settings['sale_start'] ) )? $settings['sale_start'] : $today ;
		$sale_permanent = ( isset( $settings['sale_permanent'] ) )? $settings['sale_permanent'] : false ;
?>		
		<div class="date-pickup">
			<div class="clearfix">
				<div class="row-title dib">
					<strong><?php _e( 'Enable discount permanent?', IWEBSITE_SALE_NAME ); ?></strong>
				</div>
				<div class="row-content dib">
					<label for="permanent-yes" class="b">
						<input type="radio" class="sale_permanent untouchable" name="sale_permanent[<?php echo $key; ?>]" value="1" id="permanent-yes" <?php if ( $sale_permanent ) echo 'checked="checked"'; ?> >
						<strong><?php _e( 'yes', IWEBSITE_SALE_NAME ); ?></strong>	
					</label>
					<label for="permanent-no" class="b">
						<input type="radio" class="sale_permanent untouchable" name="sale_permanent[<?php echo $key; ?>]" value="0" id="permanent-no" <?php if ( !$sale_permanent ) echo 'checked="checked"'; ?> >
						<strong><?php _e( 'no', IWEBSITE_SALE_NAME ); ?></strong>	
					</label>
				</div>
			</div>
			<div class="temporary-sale-date  <?php if( !$sale_permanent ) echo ' visible'; ?>">
				<div class="clearfix">
					<div class="row-title dib">
						<strong><?php _e( 'Enter a date when sale is started', IWEBSITE_SALE_NAME ); ?></strong>
					</div>
					<div class="row-content dib">
						<input type="date" id="start_sale_date" name="sale_start[<?php echo $key; ?>]" min="<?php echo $today; ?>" value="<?php echo $sale_start; ?>" >
					</div>
				</div>
				<div class="clearfix">
					<div class="row-title dib">
						<strong><?php _e( 'Enter a date when sale is end', IWEBSITE_SALE_NAME ); ?></strong>
					</div>
					<div class="row-content dib">
						<input type="date" id="end_sale_date" name="sale_end[<?php echo $key; ?>]" min="<?php echo $today; ?>" value="<?php echo $sale_end; ?>">
					</div>
				</div>
			</div>
		</div>
<?php	
		}



	function iwebsite_choice_category_section( $setting, $setting_key ){
		$sales_categories 		= ( isset( $setting['sales_categories'] ) )? $setting['sales_categories'] : ''; 
		$exclude_categories 	= ( isset( $setting['exclude_categories'] ) )? $setting['exclude_categories'] : false;
		$product_category_terms = get_terms( array( 'taxonomy' => 'product_cat' ) );
	?>
			<div class="active-sales-container">
				<div class="dib row-title">
					<strong><?php _e( 'Sale for selected category?', IWEBSITE_SALE_NAME ); ?></strong>
				</div>
				<div class="row-content">
					<button type="button" class="show-section button" data-key="include-category-<?php echo $setting_key;?>" ><?php _e( 'Choose category', IWEBSITE_SALE_NAME ); ?></button>
				</div>
			</div>
			<div class="all-categories-scrolling include-category-<?php echo $setting_key; ?>">
				<div class="s">
					<strong><?php _e( 'Sales categories', IWEBSITE_SALE_NAME ); ?></strong>
				</div>
				<div class="s">
				<?php
				if ( $product_category_terms  ){
					foreach ( $product_category_terms as $key => $term ) {
						$checked = ( is_array( $sales_categories ) && in_array( $term->term_id, $sales_categories ) )? 'checked="checked"' : '';
						echo '<p><label for="">';
						echo '<input type="checkbox" name="sales_categories['.$setting_key.'][]" value="'.$term->term_id.'" '.$checked.' class="untouchable">';
						echo '<span>'.$term->name.'</span>';
						echo '</label></p>';
					}
				}	
				?>
				</div>	
			</div>
	<?php		
	}

	function print_categories_names( $sales_categories ){
		$categories_names = false;
		if ( $sales_categories ){
			$categories = get_terms( array( 'taxonomy' => 'product_cat', 'include' => $sales_categories ) );
			if ( $categories ){
				$categories_names = '';
				$cat_count = count( $categories ) - 1;
				foreach ( $categories as $key => $cat ) {
					$categories_names .= $cat->name;
					if( $key != $cat_count ){
						$categories_names .= ', ';
					}
				}
			}
		}
		return $categories_names;
	}

	function iwebsite_add_image( $setting, $setting_key ){
		if ( isset( $setting['banner'] ) ){
			$discount_image = wp_get_attachment_image_src( $setting['banner'] );
?>
           <div class="sale-image">
                <div class="row-title dib <?php if ( $discount_image[0] != '' ) echo 'expand'; ?>">
                  <strong><?php _e( 'Image to frontend product:', IWEBSITE_SALE_NAME );?></strong>
                </div>
                <div class="row-content dib">
                	<div class="image-uploader">
						<span class="image-tooltip"><?php _e( 'No image selected', IWEBSITE_SALE_NAME ); ?></span>
						<img src="<?php echo $discount_image[0]; ?>" alt="" class="custom-img">
						<input type="hidden" id="custom-img" name="banner[<?php echo $setting_key; ?>]" value="<?php echo $setting['banner']; ?>">  
						<button class="set-custom-img button"><?php _e( 'Set image', IWEBSITE_SALE_NAME  ); ?></button>
						<button class="remove-custom-img button">x</button>
                	</div>
                </div>
            </div> 
<?php		
		}	
	}

	function iwebsite_active_sale( $setting, $setting_key ){
		$active_sale		= ( isset( $setting['active_sale'] ) && $setting['active_sale'] ) ? true : false;
?>
		<div class="active-sale">
			<div class="row-title dib">
				<strong><?php _e( 'Active sale', IWEBSITE_SALE_NAME ); ?></strong>
			</div>
			<div class="row-content dib">
				<label for="first_product_discount_label">
					<input type="checkbox" name="active_sale[<?php echo $setting_key; ?>]" value="true" <?php if ( $active_sale ) echo 'checked="cheked"'; ?>>
				</label>
			</div>
		</div>	
<?php		
	}




		function filter_active_sales( $sale_settings ){			
			$ar_active_sales = array();
			foreach ( $sale_settings as $key => $setting ) {
				$sale_permanent = ( isset( $setting['sale_permanent'] ) ) ?  $setting['sale_permanent'] : false;
		    	$sale_start 	= ( isset( $setting['sale_start'] ) ) ? strtotime ( $setting['sale_start'] ) : '';
		    	$sale_end 		= ( isset( $setting['sale_end'] ) )?  strtotime ( $setting['sale_end'] ) : '';
		    	if ( $sale_permanent ){
					$ar_active_sales[] = $setting;
		    	} else {
			    	if ( $sale_start == '' || $sale_end == '' ) 
			    		return false;
			    	$current_time = time();

			    	if ( $current_time > $sale_start && $current_time < $sale_end ) { 
			    		$ar_active_sales[] = $setting;
			    	}
		    	}
			}
			return $ar_active_sales;
		}


	function get_localization_domain(){
		// return CHILD_THEME_NAME;
		return '';
	}